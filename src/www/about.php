<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
	require_once("header.inc.php");
	html_head("About - PingMyDroid&trade;");
?>
<div class="contenttitle">About</div>
PingMyDroid&trade; is a solidified brainwave abnormality from
<a href="http://www.vagrearg.org">Vagrearg</a>. You may use any and all information
on this site, unless noted otherwise, under the
<a href="https://www.gnu.org/licenses/agpl-3.0.html">AGPL v3</a>
license. Software, unless noted otherwise, is made available under
<a href="http://www.gnu.org/licenses/gpl.html">GPLv3</a>.<br />
<br />
You may speak your thoughts, comments, sarcasm and other things you'd like to
get off your chest by <a href="mailto:dontspamme.pmd.at.no.spam.pingmydroid.org">sending me a message</a>
if you know how to fix an obfuscated address.
<?php
	html_bottom("");
?>
