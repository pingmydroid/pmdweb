<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
	require_once("config.inc.php");
	require_once("auth.inc.php");

	require_priv(USERPRIV_CFG);

	$hdr = "<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/jquery.datetimepicker.css\"/ >";

	html_head("Configure PingMyDroid&trade; Interface", $hdr);

function get_interfaces()
{
	global $prog_ip;
	$res = popen("$prog_ip a 2> /dev/null < /dev/null", "r");
	if(false === $res)
		return array();
	$a = array();
	$if = "unknown";
	while($line = fgets($res)) {
		if(preg_match('/^\d+:\s*([^:]+)/', $line, $m)) {
			$if = $m[1];
			if($if == "lo")
				continue;
			$a[$if] = array(array("ifs" => $if));
			continue;
		}
		if($if == "lo")
			continue;
		if(preg_match('/^\s+inet\s+(\S+)\/(\d+)/', $line, $m)) {
			$a[$if][] = array("ifs" => $if, "addr" => $m[1], "mask" => $m[2]);
			continue;
		}
		if(preg_match('/^\s+inet6\s+(\S+)\/(\d+)/', $line, $m)) {
			if(strstr($line, "deprecated"))	// Filter temporaries which are expired
				continue;
			$a[$if][] = array("ifs" => $if, "addr" => $m[1], "mask" => $m[2]);
			continue;
		}
	}
	pclose($res);
	$b = array();
	foreach($a as $v) {
		if(count($v) <= 1)
			continue;
		$b = array_merge($b, $v);
	}
	return $b;
}

?>
<script>
/* See: http://jmrware.com/articles/2009/uri_regexp/URI_regex.html */
/* Extended with port specifier ":\d{1,5}" */
var re_js_rfc3986_host_port = /^(?:\[(?:(?:(?:(?:[0-9A-Fa-f]{1,4}:){6}|::(?:[0-9A-Fa-f]{1,4}:){5}|(?:[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){4}|(?:(?:[0-9A-Fa-f]{1,4}:){0,1}[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){3}|(?:(?:[0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){2}|(?:(?:[0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})?::[0-9A-Fa-f]{1,4}:|(?:(?:[0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})?::)(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))|(?:(?:[0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})?::[0-9A-Fa-f]{1,4}|(?:(?:[0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})?::)|[Vv][0-9A-Fa-f]+\.[A-Za-z0-9\-._~!$&'()*+,;=:]+)\]|(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)|(?:[A-Za-z0-9\-._~!$&'()*+,;=]|%[0-9A-Fa-f]{2})*):\d{1,5}$/;

function ipClicked() {
	var ip4 = $("#ipv4");
	var ip6 = $("#ipv6");
	if(!ip4.is(":checked") && !ip6.is(":checked")) {
		if($(this).attr("name") == "ipv4")
			ip6.prop("checked", true);
		else
			ip4.prop("checked", true);
	}
}

function checkBounds(obj, mini, maxi) {
	if(obj.val() < mini)
		obj.val(mini);
	if(obj.val() > maxi)
		obj.val(maxi);
}

function sourceAdd(ev) {
	ev.preventDefault();
	var v = $("#sources_from option:selected");
	var o = "";
	v.each(function() {
		o += "<option value=\""+$(this).val()+"\">"+$(this).text()+"</option>\n";
		$(this).prop("selected", false);
		$(this).prop("disabled", true);
	});
	$("#sources").append(o);
}

function sourceRemove(ev) {
	ev.preventDefault();
	var v = $("#sources option:selected");
	v.each(function() {
		var tag = $("#sources_from option").filter("[value=\""+$(this).val()+"\"]");
		tag.prop("selected", false);
		tag.prop("disabled", false);
		$(this).remove();
	});
}

function targetAdd(ev) {
	ev.preventDefault();
	var v = $.trim($("#tgtval").val());
	if(!re_js_rfc3986_host_port.test(v))
		return;
	if($("#targets option[value=\""+v+"\"]").length > 0)
		return;
	$("#targets").append("<option value=\""+v+"\">"+v+"</option>\n");
}

function targetRemove(ev) {
	ev.preventDefault();
	$("#targets option:selected").remove();
}

function domainAdd(ev) {
	ev.preventDefault();
	var v = $.trim($("#domval").val());
	var domregex = /^(([0-9a-z_][0-9a-z-]*)?[a-z0-9]+\.)+[a-z]{2,}\.$/i;
	if(v.length <= 0)
		return;
	if(v[v.length-1] != '.')
		v += '.';
	if(!domregex.test(v))
		return;
	if($("#domains option[value=\""+v+"\"]").length > 0)
		return;
	$("#domains").append("<option value=\""+v+"\">"+v+"</option>\n");
}

function domainRemove(ev) {
	ev.preventDefault();
	$("#domains option:selected").remove();
}

function submitConfig(ev) {
	ev.preventDefault();
	var $dat = {
		"ipv4"			: $("#ipv4").prop("checked"),
		"ipv6"			: $("#ipv6").prop("checked"),
		"nolinklocal"		: $("#nolinklocal").prop("checked"),
		"nodns"			: $("#nodns").prop("checked"),
		"defaddr"		: $("#defaddr").prop("checked"),
		"repeat"		: $("#repeat").val(),
		"repeatinterval"	: $("#repeatinterval").val(),
		"sources"		: Array(),
		"targets"		: Array(),
		"domains"		: Array()
	};

	$("#sources option").each(function(ix) { $dat["sources"][ix] = $(this).val();});
	$("#targets option").each(function(ix) { $dat["targets"][ix] = $(this).val();});
	$("#domains option").each(function(ix) { $dat["domains"][ix] = $(this).val();});
	$.post("/rest/setconfig", $dat, function(d, s, jq) {
		/* Success */
		$("#errorText").stop(true, true).text(d).show().fadeOut(15000);
	}).fail(function(d) {
		/* Failure */
		$("#errorText").stop(true, true).text("Error: "+d.responseText).show().fadeOut(15000);
	});
}

$(document).ready(function() {
	$(".ip").on("click", ipClicked);
	$("#repeat").on("change", function() { checkBounds($(this), 1, 16);});
	$("#repeatinterval").on("change", function() { checkBounds($(this), 0, 10000);});
	$("#srcrem").on("click", sourceRemove);
	$("#srcadd").on("click", sourceAdd);
	$("#tgtrem").on("click", targetRemove);
	$("#tgtadd").on("click", targetAdd);
	$("#domrem").on("click", domainRemove);
	$("#domadd").on("click", domainAdd);
	$("#configform").on("submit", submitConfig);
});

</script>
<noscript>
 <br />
 <div>JavaScript is (unfortunately) required for message generation and submission. Please enable JavaScript for this page to continue.</div>
 <br />
</noscript>
<div class="contenttitle">Configure PingMyDroid&trade; Interface</div>
<!-- <div class="contentsubtitle">...</div> -->
<form id="configform">
<table id="config">
<tr>
 <td>Use IPv4</td>
 <td><input class="ip" id="ipv4" type="checkbox" name="ipv4" value="1"<?php echo ($cfg['ipv4'] ? " checked=\"checked\"" : "");?> title="Use IPv4 transport for sending PMD messages"/></td>
</tr>
<tr>
 <td>Use IPv6</td>
 <td><input class="ip" id="ipv6" type="checkbox" name="ipv6" value="1"<?php echo ($cfg['ipv6'] ? " checked=\"checked\"" : "");?> title="Use IPv6 transport for sending PMD messages"/></td>
</tr>
<tr>
 <td>Do not use Link Local addresses</td>
 <td><input id="nolinklocal" type="checkbox" name="nolinklocal" value="1"<?php echo ($cfg['nolinklocal'] ? " checked=\"checked\"" : "");?> title="Do not use network interface''s link-local addresed while sending PMD messages"/></td>
</tr>
<tr>
 <td>Do not use DNS resolution</td>
 <td><input id="nodns" type="checkbox" name="nodns" value="1"<?php echo ($cfg['nodns'] ? " checked=\"checked\"" : "");?> title="Do not use DNS to discover services and domains"/></td>
</tr>
<tr>
 <td>Add default PMD address:port</td>
 <td><input id="defaddr" type="checkbox" name="defaddr" value="1"<?php echo ($cfg['defaddr'] ? " checked=\"checked\"" : "");?> title="Always add default PMD IPv4/IPv6 address and port for sending PMD messages"/></td>
</tr>
<tr>
 <td>Message repeat count</td>
 <td><input id="repeat" type="number" name="repeat" value="<?php echo htmlentities($cfg['repeat']);?>" title="Set number of message repeats with incrementing sequence [1..16]"/></td>
</tr>
<tr>
 <td>Interval between repeats [ms]</td>
 <td><input id="repeatinterval" type="number" name="repeat" value="<?php echo htmlentities($cfg['repeatinterval']);?>" title="Set the time between resending same message in milliseconds [0..10000]"/></td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr>
 <td>Source address/interface</td>
 <td>
  <table>
   <tr>
    <td rowspan="2">
     <select multiple="multiple" id="sources_from" name="sources_from" title="Source addresses or interfaces for sending packets">
      <optgroup label="Available addresses">
<?php
	$ifs = get_interfaces();
	$ifsel = array();
	$ifunsel = array();
	foreach($ifs as $if) {
		$found = false;
		foreach($cfg['sources'] as $src) {
			if($if['ifs'] == $src || $if['addr'] == $src) {
				$ifsel[] = $if;
				$found = true;
				break;
			}
		}
		if(!$found)
			$ifunsel[] = $if;
	}

	foreach($ifs as $k => $if) {
		$found = in_array($if, $ifsel);
		$dsp = $found ? 'disabled="disabled"' : "";
		if(isset($if['addr'])) {
			$opt = $if['ifs'] . ': ' . $if['addr'] . '/' . $if['mask'];
			$val = $if['addr'];
		} else {
			$opt = $if['ifs'];
			$val = $if['ifs'];
		}
		echo "<option $dsp value=\"".htmlentities($val)."\">".htmlentities($opt)."</option>\n";
	}
?>
      </optgroup>
     </select>
    </td>
    <td><input type="image" id="srcadd" src="/image/tasto-right.png" width="22" height="23" alt="Add source" title="Add source" /></td>
    <td rowspan="2">
     <select id="sources" multiple="multiple" name="sources" title="Source addresses or interfaces for sending packets">
<?php
	foreach($cfg['sources'] as $src) {
		$s = htmlentities($src);
		echo "<option value=\"$s\">$s</option>\n";
	}
?>
     </select>
    </td>
   </tr>
   <tr>
    <td><input type="image" id="srcrem" src="/image/tasto-left.png" width="22" height="23" alt="Remove source" title="Remove source" /></td>
   </tr>
  </table>
 </td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr>
 <td>Destination address:port</td>
 <td>
  <table>
   <tr>
    <td>
     <select id="targets" name="targets" multiple="multiple" title="Destination addresses and ports for sending packets">
<?php
	foreach($cfg['targets'] as $src) {
		$s = htmlentities($src);
		echo "<option value=\"$s\">$s</option>\n";
	}
?>
     </select>
    </td>
    <td><input type="image" id="tgtrem" src="/image/tasto-x-int.png" width="23" height="23" alt="Remove target" title="Remove target" /></td>
   </tr>
   <tr>
    <td><input id="tgtval" name="tgtadd" type="text" value="" title="Enter IPv4 address:port or IPv6 [address]:port to add" /></td>
    <td><input type="image" id="tgtadd" src="/image/tasto-plus.png" width="23" height="23" alt="Add target" title="Add target" /></td>
   </tr>
  </table>
 </td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr>
 <td>Domain bindings</td>
 <td>
  <table>
   <tr>
    <td>
     <select id="domains" name="domains" multiple="multiple" title="Domains to bind to for SRV resolution">
<?php
	foreach($cfg['domains'] as $src) {
		$s = htmlentities($src);
		echo "<option value=\"$s\">$s</option>\n";
	}
?>
     </select>
    </td>
    <td><input type="image" id="domrem" src="/image/tasto-x-int.png" width="23" height="23" alt="Remove domain" title="Remove domain" /></td>
   </tr>
   <tr>
    <td><input id="domval" name="domadd" type="text" value="" title="Enter domain name to add" /></td>
    <td><input type="image" id="domadd" src="/image/tasto-plus.png" width="23" height="23" alt="Add domain" title="Add domain" /></td>
   </tr>
  </table>
 </td>
</tr>
<tr>
 <td><input class="submit" type="submit" name="submit" value="Change Configuration"/></td>
 <td id="errorText" style="color:red"></td>
</tr>
</table>
</form>
Notes:
<ul>
 <li>It is not possible to disable both IPv4 and IPv6</li>
 <li>Selecting no sources implies sending from all interfaces and addresses found on the system</li>
 <li>Selected link-local addresses are not used if the use of link-local addresses is disabled</li>
 <li>Domain bindings are only considered if DNS queries are enabled</li>
</ul>
<?php
	html_bottom("");
?>
