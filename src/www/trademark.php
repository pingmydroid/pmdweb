<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
	require_once("header.inc.php");
	html_head("Documentation - PingMyDroid&trade;");
?>
<div class="contenttitle">Trademark &trade;</div>
I always wanted to use that &trade; sign and finally found a way to use it in a
funny yet real way.<br />
<ul>
<li>The trademark PingMyDroid&trade; may be used in the form "PingMyDroid&trade;
Compatible" or "PingMyDroid&trade; Compliant" by anyone who distributes
software implementing the PMD protocol if the implementation is fully compliant
with the PMD protocol as described in the
<a href="http://www.pingmydroid.org/content/protocol">PingMyDroid&trade; protocol memo</a>.</li>
<li>Anybody is free to use the abbreviated protocol identifier "PMDP" as (s)he sees
fit.</li>
<li>Incidental use or "for reference" use is allowed, but should be accompanied
with a source reference.</li>
<li>I don't have any lawyers employed (yet). Please keep it that way.</li>
</ul>
For uses beyond the above scope, contact
<a href="http://www.vagrearg.org">Vagrearg</a> for possibilities.<br />
<?php
	html_bottom("");
?>
