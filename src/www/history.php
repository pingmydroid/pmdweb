<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
	require_once("auth.inc.php");

	require_priv(USERPRIV_HIST);

	$hdr = "<script src=\"/js/pmdcommon.js\"></script>";

	html_head("History of PingMyDroid&trade; Messages", $hdr);

	$resend = has_priv(USERPRIV_SEND);
?>
<script>

var lastorder = 0;

function getVars() {
	$.get("/rest/categories", function(resp) { cats  = resp; });
	$.get("/rest/subcategories", function(resp) { subcats  = resp; });
}

function imgR() {
	return <?php if($resend) { ?>'<input type="image" class="resend" alt="Resend Historic Message" title="Resend Historic Message" width="23" height="23" src="/image/resend.png" />' <?php } else { ?> '' <?php } ?>;
}

function fillHistory(order) {
	$("#history").empty();
	$("#history").append("<tr>"
			+ "<th class=\"dropdown\" id=\"srtid\" title=\"Click to sort\">ID</th>"
			+ "<th class=\"dropdown\" id=\"srtusr\" title=\"Click to sort\">User</th>"
			+ "<th class=\"dropdown\" id=\"srtts\" title=\"Click to sort\">Timestamp</th>"
			+ "<th class=\"dropdown\" id=\"srtsev\" colspan=\"2\" title=\"Click to sort\">Severity</th>"
			+ "<th>Silent</th>"
			+ "<th>Signed</th>"
			+ "<th class=\"dropdown\" id=\"srtuuid\" title=\"Click to sort\">Message UUID</th>"
			+ "<th class=\"dropdown\" id=\"srtsent\" title=\"Click to sort\">Sent on</th></tr>\n");
	$("#srtid").on("click", function() { fillHistory(0); });
	$("#srtts").on("click", function() { fillHistory(1); });
	$("#srtsev").on("click", function() { fillHistory(2); });
	$("#srtuuid").on("click", function() { fillHistory(3); });
	$("#srtsent").on("click", function() { fillHistory(4); });
	$("#srtusr").on("click", function() { fillHistory(5); });
	lastorder = order;
	$.get("/rest/history", { "order": order }, function(resp) {
		var s = 0;
		$("msg", resp).each(function(c) {
			var shade = (s & 1) ? " class=\"shaded\"" : "";
			s++;
			var sig = $("sigfp", this).text().length > 0 ? "true" : "false";
			var $row = "<tr" + shade + " id=\"" + $("id", this).text() + "\">"
				+ "<td class=\"dropdown\">" + $("id", this).text() + "</td>"
				+ "<td class=\"dropdown\">" + $("usr", this).text() + "</td>"
				+ "<td class=\"dropdown\">" + $("ts", this).text().replace(/ /g, "&nbsp;") + "</td>"
				+ "<td class=\"dropdown\">" + severityImage($("severityid", this).text()) + "</td>"
				+ "<td class=\"dropdown\">" + $("severity", this).text().replace(/ /g, "&nbsp;") + "</td>"
				+ "<td class=\"dropdown\">" + $("forcesilent", this).text() + "</td>"
				+ "<td class=\"dropdown\">" + sig + "</td>"
				+ "<td class=\"dropdown\" style=\"font-family:monospace\">" + $("uniid", this).text().replace(/-/g, "&#8209;") + "</td>"
				+ "<td class=\"dropdown\">" + $("sent", this).text().replace(/ /g, "&nbsp;") + "</td>"
				+ "<td>" + imgR() + "</td>"
				+ "</tr>\n";
			$("#history").append($row);
			$("#history").append("<tr style=\"display:none\"><td></td><td colspan=\"8\"></td></tr>\n");
		});
	});
}

function historyClicked() {
	var trid = $(this).parent().children().eq(0).text();
	var tr = $(this).parent().next();
	if(tr.css("display") === "none") {
		tr.css("display", "visible");
		var td = tr.children().eq(1);
		if(!td.children().length) {
			/* No content yet, get the history */
			var c = "<table\n";
			c += "<tr><th>Seq</th><th>Type</th><th>Content</th></tr>\n";
			$.get("/rest/historymessage", { "msgid": trid}, function(resp) {
				var mycat = -1;
				$("content", resp).each(function() {
					c += "<tr>";
					c += "<td>"+$("seq", this).text()+"</td>";
					c += "<td>"+$("description", this).text()+"</td>";
					switch($("typ", this).text()) {
					case "0":	// Category
						var val = mycat = $("u_int1", this).text();
						c += "<td>"+$("category[id='"+val+"'] description", cats).text()+" ("+val+")</td>";
						break;
					case "1":	// Subcategory
						var val = $("u_int1", this).text();
						if(mycat != -1)
							c += "<td>"+$("subcategory[id='"+val+"'][cat='"+mycat+"'] description", subcats).text()+" ("+val+")</td>";
						else
							c += "<td>"+val+"</td>";
						break;
					case "2":	// Text
						c += "<td>"+$("u_txt", this).text()+"</td>";
						break;
					case "3":	// URI
						c += "<td><a href=\""+$("u_txt", this).text()+"\">"+$("u_txt", this).text()+"</a></td>";
						break;
					case "4":	// UUID
						c += "<td>"+$("u_txt", this).text()+"</td>";
						break;
					case "5":	// Timestamp
						c += "<td>"+$("u_ts", this).text()+"</td>";
						break;
					case "6":	// Cardinal
						c += "<td>"+$("u_int1", this).text()+"</td>";
						break;
					case "7":	// Integer
						c += "<td>"+$("u_int1", this).text()+"</td>";
						break;
					case "8":	// Fraction
						var nom = $("u_int1", this).text();
						var div = $("u_int2", this).text();
						var res = "";
						if(div === "0") {
							if(nom === "0")
								res = "NaN";
							else if(nom < 0)
								res = "-Infinite";
							else
								res = "+Infinite";
						} else
							res = nom / div;
						c += "<td>"+nom+"&nbsp;/&nbsp;"+div+" ("+res+")</td>";
						break;
					}
					c += "</tr>\n";
				});
				c += "</table\n";
				td.append(c);
			});
		}
	} else {
		tr.css("display", "none");
	}
}

function resendClicked() {
	var dat = { "id" : $(this).parentsUntil("tr").parent().attr("id") };
	$.get("/rest/resend", dat, function() { fillHistory(lastorder); });
}

$(document).ready(function() {
	getVars();
	fillHistory(lastorder);
	$("#history").on("mouseover", ".dropdown", function() {
		$(this).css("text-decoration", "underline");
	});
	$("#history").on("mouseleave", ".dropdown", function() {
		$(this).css("text-decoration", "none");
	});
	$("#history").on("click", ".dropdown", historyClicked);
	$("#history").on("click", ".resend", resendClicked);
});

</script>
<noscript>
 <br />
 <div>JavaScript is (unfortunately) required for message generation and submission. Please enable JavaScript for this page to continue.</div>
 <br />
</noscript>
<div class="contenttitle">History of PingMyDroid&trade; Messages</div>
<!-- <div class="contentsubtitle">...</div> -->
<input type="button" value="Reload history" onClick="location.reload();" />
<table id="history">
</table>
<input type="button" value="Reload history" onClick="location.reload();" />
<?php
	html_bottom("");
?>
