<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
	require_once("auth.inc.php");

	/* We do not require special privs here because the user should be able to change password */
	require_priv(0);

	html_head("Configure PingMyDroid&trade; Users");

	$dis = has_priv(USERPRIV_CFGUSR) ? "" : ' disabled=\"disabled\"';

	echo "<script>\n";
	echo "var privsbyid = [\n";
	foreach($userprivs as $v)
		echo sprintf("'%d',\n", $v['val']);
	echo "];\n";
	echo "var privsbyname = [\n";
	foreach($userprivs as $v)
		echo sprintf("'%s',\n", $v['name']);
	echo "];\n";
	echo "var privsbyabbr = [\n";
	foreach($userprivs as $v)
		echo sprintf("'%s',\n", $v['abbr']);
	echo "];\n";
	echo "</script>\n";
?>
<script>
function imgX() {
	return '<input type="image" class="remuser" alt="Remove User" title="Remove User" width="23" height="23" src="/image/tasto-x-int.png" />';
}

function imgP() {
	return '<input type="image" class="adduser" alt="Add User" title="Add User" width="23" height="23" src="/image/tasto-plus.png" />';
}

function fillUsers() {
	$("#userlist tr").remove();
	$("#userlist").append("<tr><th>ID</th><th>Priveleges</th><th></th></tr>\n");
	$.get("/rest/getusers", function(resp) {
		$("user", resp).each(function(c) {
			var ul = $("#userlist");
			var priv = $("priv", this).text();
			var uid = $("id", this).text();
			var o = "";
			var op = "";
			for(var i = 0; i < privsbyid.length; i++) {
				o += "<option value=\"" + privsbyid[i] + "\"";
				if(priv & privsbyid[i]) {
					o += " selected=\"selected\"";
					op += "," + privsbyabbr[i];
				}
				o += ">" + privsbyname[i] + "</option>\n";
			}
			op = '[' + op.substr(1) + ']';
			var tr = "<tr>"
				+ "<td class=\"dropdown\">" + uid + "</td>\n"
				+ "<td class=\"dropdown\">" + op + "</td>\n"
				+ "<td>" + (uid == "admin" ? "" : imgX()) + "</td>\n"
				+ "</tr>\n";
			ul.append(tr);
			var tr = "<tr style=\"display:none\" class=\"hide\"><td colspan=\"3\"><table id=\"" + uid + "\">"
				+ "<tr><td>Priveleges</td><td><select<?php echo $dis; ?> multiple=\"multiple\">\n"
				+ o
				+ "</select></td></tr>\n"
				+ "<tr><td></td><td><input<?php echo $dis; ?> class=\"privchange\" type=\"button\" name=\"changepriv\" value=\"Change priveleges\" /></td></tr>\n"
				+ "<tr><td>New password</td><td><input type=\"password\" name=\"password\" value=\"\" /></td></tr>\n"
				+ "<tr><td>Retype new password</td><td><input type=\"password\" name=\"password2\" value=\"\" /></td></tr>\n"
				+ "<tr><td></td><td><input class=\"pwchange\" type=\"button\" name=\"changepwd\" value=\"Change password\" /></td></tr>\n"
				+ "<tr><td colspan=\"2\"><hr /></td></tr>\n"
				+ "</table></td></tr>\n";
			ul.append(tr);
		});
		$("#userlist").append("<tr><td></td><td></td><td>"+imgP()+"</td></tr>");
		var tr = "<tr style=\"display:none\" class=\"hide\"><td colspan=\"3\"><table>"
			+ "<tr><td colspan=\"2\"><hr /></td></tr>\n"
			+ "<tr><td colspan=\"2\">Add New User:</td></tr>\n"
			+ "<tr><td>Username</td><td><input type=\"text\" name=\"username\" value=\"\" /></td></tr>\n"
			+ "<tr><td>Priveleges</td><td><select multiple=\"multiple\">\n";
			for(var i = 0; i < privsbyid.length; i++)
				tr += "<option value=\"" + privsbyid[i] + "\">" + privsbyname[i] + "</option>\n";
			tr += "</select></td></tr>\n"
			+ "<tr><td>Password</td><td><input type=\"password\" name=\"password\" value=\"\" /></td></tr>\n"
			+ "<tr><td>Retype password</td><td><input type=\"password\" name=\"password2\" value=\"\" /></td></tr>\n"
			+ "<tr><td></td><td><input class=\"useradd\" type=\"button\" name=\"adduser\" value=\"Add User\" /></td></tr>\n"
			+ "</table></td></tr>\n";
		$("#userlist").append(tr);
	});
}

function userClicked() {
	var tr = $(this).parent().next();
	if(tr.css("display") === "none") {
		$("#userlist .hide").css("display", "none");
		tr.css("display", "visible");
	} else {
		tr.css("display", "none");
	}
}

function errorInput(s) {
	$("#errorInputText").stop(true, true).text(s).show().fadeOut(15000);
}

function pwchangeClicked() {
	var p = $(this).parentsUntil("table");
	var i = $("input", p);
	var uid = p.parent().attr("id");
	var pw1 = i.filter("[name=\"password\"]").val();
	var pw2 = i.filter("[name=\"password2\"]").val();
	if(pw1.length <= 0) {
		errorInput("Password for '"+uid+"' should not be empty");
		return;
	}
	if(pw1 != pw2) {
		errorInput("Passwords for '"+uid+"' do not match");
		return;
	}
	var dat = { "uid" : uid, "pwd" : pw1 };
	$.get("/rest/updatepwd", dat, function(d, s, jq) {
		errorInput("Password for '"+uid+"' updated");
	}).fail(function() {
		/* Failure */
		errorInput("Password update error");
	});
}

function privchangeClicked() {
	var p = $(this).parentsUntil("table");
	var uid = p.parent().attr("id");
	var priv = 0;
	$("select option:selected", p).each(function() {
		priv |= $(this).val();
	});
	var dat = { "uid" : uid, "priv" : priv };
	$.get("/rest/updatepriv", dat, function(d, s, jq) {
		errorInput("Priveleges for '"+uid+"' updated");
		fillUsers();
	}).fail(function() {
		/* Failure */
		errorInput("Privelege update error");
	});
}

function addClicked() {
	$("#userlist .hide").css("display", "none");
	$(this).parentsUntil("table").next().css("display", "visible");
}

function useraddClicked() {
	var p = $(this).parentsUntil("table");
	var i = $("input", p);
	var uid = i.filter("[name=\"username\"]").val();
	var pw1 = i.filter("[name=\"password\"]").val();
	var pw2 = i.filter("[name=\"password2\"]").val();
	if(!/^[a-zA-Z][a-zA-Z0-9_.-]+/.test(uid)) {
		errorInput("Invalid username");
		return;
	}
	if(pw1.length <= 0) {
		errorInput("Password for '"+uid+"' should not be empty");
		return;
	}
	if(pw1 != pw2) {
		errorInput("Passwords for '"+uid+"' do not match");
		return;
	}
	var priv = 0;
	$("select option:selected", p).each(function() {
		priv |= $(this).val();
	});
	var dat = { "uid" : uid, "priv" : priv, "pwd" : pw1 };
	$.get("/rest/newuser", dat, function(d, s, jq) {
		errorInput("New user '"+uid+"' created");
		fillUsers();
	}).fail(function() {
		/* Failure */
		errorInput("User create error");
	});
}

function removeClicked() {
	var uid = $(this).parent().parent().children().eq(0).text();
	if(!confirm("You are removing user '"+uid+"' permanently.\n This cannot be undone. Proceed?"))
		return;
	var dat = { "uid" : uid };
	$.get("/rest/removeuser", dat, function(d, s, jq) {
		errorInput("User '"+uid+"' removed");
		fillUsers();
	}).fail(function() {
		errorInput("User remove error");
	});
}

$(document).ready(function() {
	fillUsers();
	$("#userlist").on("mouseover", ".dropdown", function() {
		$(this).css("text-decoration", "underline");
	});
	$("#userlist").on("mouseleave", ".dropdown", function() {
		$(this).css("text-decoration", "none");
	});
	$("#userlist").on("click", ".dropdown", userClicked);
	$("#userlist").on("click", ".remuser", removeClicked);
	$("#userlist").on("click", ".adduser", addClicked);
	$("#userlist").on("click", ".pwchange", pwchangeClicked);
	$("#userlist").on("click", ".privchange", privchangeClicked);
	$("#userlist").on("click", ".useradd", useraddClicked);
});

</script>
<noscript>
 <br />
 <div>JavaScript is (unfortunately) required for message generation and submission. Please enable JavaScript for this page to continue.</div>
 <br />
</noscript>
<div class="contenttitle">Configure PingMyDroid&trade; Users</div>
<table id="userlist">
</table>
<br />
<div id="errorInputText" style="float:left; color:red"></div>
<?php
	html_bottom("");
?>
