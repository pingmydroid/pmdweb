<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
	require_once("auth.inc.php");

	require_priv(USERPRIV_SEND);

	$hdr = "<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/jquery.datetimepicker.css\"/ >\n"
		."<script src=\"/js/jquery.datetimepicker.js\"></script>\n"
		."<script src=\"/js/pmdcommon.js\"></script>";

	html_head("Send a PingMyDroid&trade; Message", $hdr);

?>
<script>
/* URI regex to match all valid URI strings */
/* See: http://jmrware.com/articles/2009/uri_regexp/URI_regex.html */
var re_js_rfc3986_URI = /^[A-Za-z][A-Za-z0-9+\-.]*:(?:\/\/(?:(?:[A-Za-z0-9\-._~!$&'()*+,;=:]|%[0-9A-Fa-f]{2})*@)?(?:\[(?:(?:(?:(?:[0-9A-Fa-f]{1,4}:){6}|::(?:[0-9A-Fa-f]{1,4}:){5}|(?:[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){4}|(?:(?:[0-9A-Fa-f]{1,4}:){0,1}[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){3}|(?:(?:[0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){2}|(?:(?:[0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})?::[0-9A-Fa-f]{1,4}:|(?:(?:[0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})?::)(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))|(?:(?:[0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})?::[0-9A-Fa-f]{1,4}|(?:(?:[0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})?::)|[Vv][0-9A-Fa-f]+\.[A-Za-z0-9\-._~!$&'()*+,;=:]+)\]|(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)|(?:[A-Za-z0-9\-._~!$&'()*+,;=]|%[0-9A-Fa-f]{2})*)(?::[0-9]*)?(?:\/(?:[A-Za-z0-9\-._~!$&'()*+,;=:@]|%[0-9A-Fa-f]{2})*)*|\/(?:(?:[A-Za-z0-9\-._~!$&'()*+,;=:@]|%[0-9A-Fa-f]{2})+(?:\/(?:[A-Za-z0-9\-._~!$&'()*+,;=:@]|%[0-9A-Fa-f]{2})*)*)?|(?:[A-Za-z0-9\-._~!$&'()*+,;=:@]|%[0-9A-Fa-f]{2})+(?:\/(?:[A-Za-z0-9\-._~!$&'()*+,;=:@]|%[0-9A-Fa-f]{2})*)*|)(?:\?(?:[A-Za-z0-9\-._~!$&'()*+,;=:@\/?]|%[0-9A-Fa-f]{2})*)?(?:\#(?:[A-Za-z0-9\-._~!$&'()*+,;=:@\/?]|%[0-9A-Fa-f]{2})*)?$/;

/* UUID regex */
var re_js_UUID = /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/;

/* Date regex for YYYY-MM-DD HH:MM:SS */ 
var re_js_DATE = /^\s*[0-9]{4}-(1[012])|(0?[1-9])-(3[01])|([1-2][0-9])|(0?[1-9])\s+([01]?[0-9])|(2[0-3]):[0-5]?[0-9](:[0-5]?[0-9])?\s*$/;

function imgX()
{
	return '<img class="remove" alt="Remove" title="Remove" width="23" height="23" src="/image/tasto-x-int.png" />';
}

function imgUp()
{
	return '<img class="up" alt="Move-Up" title="Move Up" width="23" height="22" src="/image/tasto-up.png" />';
}

function imgDown()
{
	return '<img class="down" alt="Move-Down" title="Move Down" width="23" height="22" src="/image/tasto-down.png" />';
}

function fillSeverity()
{
	$.get("/rest/severities", function(resp) {
		$("#severity").append("<option selected=\"selected\" value=\"-1\">-- Select Severity --</option>");
		$("#severityValue").val("-1");
		$("severity", resp).each(function(c) {
			$("#severity").append("<option value=\"" + $("id", this).text() + "\">" + $("description", this).text() + "</option>");
		});
	});
}

function fillCategory()
{
	$.get("/rest/categories", function(resp) {
		$("#category").append("<option selected=\"selected\" value=\"-1\">-- Select Category --</option>");
		$("#categoryValue").val("-1");
		$("category", resp).each(function(c) {
			$("#category").append("<option value=\"" + $("id", this).text() + "\">" + $("description", this).text() + "</option>");
		});
		$("#category").append("<option value=\"-2\">-- Custom Value --</option>");
	});
}

function updateCategory()
{
	var idx = this[this.selectedIndex].value;

	if(idx == -2) {
		/* Index -2 is custom value */
		$("#customCategory").css("display", "visible");		// Show the custom entry fields
		$("#customSubcategory").css("display", "visible");
		$("#subcategory").prop("disabled", true);		// Disable subcat selection
		$("#categoryValue").val("0");
		$("#subcategoryValue").val("0");
		checkCategory();
	} else {
		$("#customCategory").css("display", "none");		// Hide custom entry fields
		$("#customSubcategory").css("display", "none");
		$("#subcategory").prop("disabled", false);		// Enable subcat selection
		$("#categoryValue").val(idx);
		$("#subcategoryValue").val("-1");

		/* Fill the sub-category drop down with new options */
		$("#subcategory option").eq(0).prop('selected', true);
		$("#subcategory").empty();
		$("#subcategory").append("<option selected=\"selected\" value=\"-1\">-- Select Sub-category --</option>");
		$.get("/rest/subcategories", { "category": idx}, function(resp) {
			$("subcategory", resp).each(function(c) {
				$("#subcategory").append("<option value=\"" + $("id", this).text() + "\">" + $("description", this).text() + "</option>");
			});
			$("#subcategory").append("<option value=\"-2\">-- Custom Value --</option>");
		});
		checkCategory();
	}
}

function updateSeverity()
{
	var idx = this[this.selectedIndex].value;
	if(idx < 0)
		$("#severityText").css("color", "red");
	else
		$("#severityText").css("color", "black");
	$("#severityImg").html(severityImage(idx));
}

function updateSubcategory()
{
	var idx = this[this.selectedIndex].value;
	if(idx == -2) {
		$("#customSubcategory").css("display", "visible");
		$("#subcategoryValue").val("0");
	} else {
		$("#customSubcategory").css("display", "none");
		$("#subcategoryValue").val(idx);
	}
	checkCategory();
}

function checkCategory()
{
	if($("#categoryValue").val() < 0)
		$("#categoryText").css("color", "red");
	else
		$("#categoryText").css("color", "black");

	if($("#subcategoryValue").val() < 0)
		$("#subcategoryText").css("color", "red");
	else
		$("#subcategoryText").css("color", "black");
}

var $cntcnt = 0;
function addContent(selector)
{
	var $imgs = "<td>" + imgX() + "</td><td>" + imgDown() + "</td><td>" + imgUp() + "</td>";
	var $ct = $("#contentTable");
	switch($("#addContent").val()) {
	case "text":
		var $id = "content_text_" + $cntcnt;
		$ct.append("<tr class=\"text\"><td>Text message</td><td><input type=\"text\" name=\""+$id+"\" size=\"64\"/></td><td></td>"+$imgs+"</tr>\n");
		break;
	case "resource":
		var $id = "content_resource_" + $cntcnt;
		$ct.append("<tr class=\"resource\"><td>URI/URL resource</td><td><input type=\"text\" name=\""+$id+"\" size=\"64\"/></td><td></td>"+$imgs+"</tr>\n");
		break;
	case "integer":
		var $id = "content_integer_" + $cntcnt;
		$ct.append("<tr class=\"integer\"><td>Integer value</td><td><input class=\"integer\" value=\"0\" type=\"number\" name=\""+$id+"\" size=\"20\"/></td><td class=\"integer\"></td>"+$imgs+"</tr>\n");
		$("input.integer").each(updateInteger);
		break;
	case "cardinal":
		var $id = "content_cardinal_" + $cntcnt;
		$ct.append("<tr class=\"cardinal\"><td>Unsigned value</td><td><input class=\"cardinal\" value=\"0\" type=\"number\" name=\""+$id+"\" size=\"20\"/></td><td class=\"cardinal\"></td>"+$imgs+"</tr>\n");
		$("input.cardinal").each(updateCardinal);
		break;
	case "fraction":
		var $id = "content_fraction_" + $cntcnt;
		$ct.append("<tr class=\"fraction\"><td>Fractional value</td><td><input class=\"fraction\" value=\"0\" type=\"number\" name=\""+$id+"_den\" size=\"20\"/>/<input class=\"fraction\" value=\"0\" type=\"number\" name=\""+$id+"_div\" size=\"20\"/></td><td class=\"fraction\"></td>"+$imgs+"</tr>\n");
		$("input.fraction").each(updateFraction);
		break;
	case "uuid":
		var $id = "content_uuid_" + $cntcnt;
		$ct.append("<tr class=\"uuid\"><td class=\"uuid\">Universal Unique Identifier</td><td><input class=\"uuid\"type=\"text\" name=\""+$id+"\" size=\"36\" maxlength=\"36\" /></td><td></td>"+$imgs+"</tr>\n");
		$("input.uuid").each(updateUUID);
		break;
	case "timestamp":
		var $id = "content_timestamp_" + $cntcnt;
		$ct.append("<tr class=\"timestamp\"><td>Timestamp (YYYY-mm-dd HH:MM[:SS])</td><td><input id=\"datetimepicker\" type=\"text\" name=\""+$id+"\"/></td><td></td>"+$imgs+"</tr>\n");
		$('#datetimepicker').datetimepicker({format: 'Y-m-d H:i:s', step:30});
		break;
	}
	$cntcnt++;
	$("#addContent option").eq(0).prop('selected', true);
	updateArrows()
}

function updateArrows()
{
	var $ct = $("#contentTable tr");
	$ct.each(function(x) {
		$(this).find(".up").css("display", "visible");
		$(this).find(".down").css("display", "visible");
	});
	$ct.first().find(".up").css("display", "none");
	$ct.last().find(".down").css("display", "none");
}

function upClicked()
{
	var tag = $(this).closest("tr");
	tag.prev("tr").before(tag.clone());
	tag.remove();
	updateArrows()
}

function downClicked()
{
	var tag = $(this).closest("tr");
	tag.next("tr").after(tag.clone());
	tag.remove();
	updateArrows()
}

function removeClicked()
{
	$(this).closest("tr").remove();
	updateArrows()
}

function updateFraction()
{
	var ip = $("input.fraction", $(this).parent());
	var td = $("td.fraction", $(this).closest("tr"));
	var den = ip.eq(0).val();
	var div = ip.eq(1).val();
	if(den == 0 && div == 0) {
		td.html("(NaN)");
	} else if(div == 0) {
		if(den > 0)
			td.html("(+Infinite)");
		else
			td.html("(-Infinite)");
	} else {
		td.html("(" + den/div + ")");
	}
}

function updateInteger()
{
	var ip = $("input.integer", $(this).parent()).eq(0);
	var td = $("td.integer", $(this).closest("tr"));
	var ival = parseInt(ip.val());
	td.html("(" + ival.toString(10) + " = hex:" + ival.toString(16) + ")");
}

function updateCardinal()
{
	var ip = $("input.cardinal", $(this).parent()).eq(0);
	var td = $("td.cardinal", $(this).closest("tr"));
	var ival = parseInt(ip.val());
	if(ival < 0) {
		errorText("Warning: Cardinal values must be >= zero, set to absolute value");
		ival = -ival;
		ip.val(ival);
	}
	td.html("(" + ival.toString(10) + " = hex:" + ival.toString(16) + ")");
}

function updateUUID()
{
	var ip = $("input.uuid", $(this).parent()).eq(0);
	var td = $("td.uuid", $(this).closest("tr"));
	if(!re_js_UUID.test($("input", this).val()))
		td.css("color", "red");
	else
		td.css("color", "black");
}

function errorText(t)
{
	$("#errorText").stop(true, true).text(t).show().fadeOut(10000);
}

function submitData()
{
	var $cat = $("#categoryValue").val();
	var $scat = $("#subcategoryValue").val();
	var $cnt = 0;
	var $dat = {
		"severity"	: $("#severity option:selected").val(),
		"forcesilent"	: $("#forceSilent").prop("checked"),
		"content"	: Array()
	};
	if($dat["severity"] < 0) {
		errorText("Error: Severity must be set");
		$("#severity").focus();
		return;
	}
	if($dat["severity"] == 0) {
		if(!confirm("You are about to send an emergency message.\nAre you sure the situation is an emergency?"))
			return;
	}
	if($cat < 0) {
		errorText("Error: Category must be set");
		$("#category").focus();
		return;
	}
	if($scat < 0) {
		errorText("Error: Sub-category must be set");
		$("#subcategory").focus();
		return;
	}

	$dat["content"][$cnt++] = {"category": $cat};
	$dat["content"][$cnt++] = {"subcategory": $scat};

	var $err = false;
	$("#contentTable tr").each(function(ix) {
		switch($(this).attr("class")) {
		case "text":
			$dat["content"][$cnt++] = {"text": $("input", this).val()};
			break;
		case "resource":
			var $txt = $("input", this).val();
			if($txt.length > 0 && !re_js_rfc3986_URI.test($txt)) {
				errorText("Error: Resource URI format error");
				$("input", this).focus();
				$err = true;
				return;
			}
			$dat["content"][$cnt++] = {"resource": $txt};
			break;
		case "integer":
			$dat["content"][$cnt++] = {"integer": $("input", this).val()};
			break;
		case "cardinal":
			$dat["content"][$cnt++] = {"cardinal": $("input", this).val()};
			break;
		case "fraction":
			$dat["content"][$cnt++] = {"fraction": [$("input", this).eq(0).val(), $("input", this).eq(1).val()]};
			break;
		case "uuid":
			if(!re_js_UUID.test($("input", this).val())) {
				errorText("Error: UUID format error, expected xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx");
				$("input", this).focus();
				$err = true;
				return;
			}
			$dat["content"][$cnt++] = {"uuid": $("input", this).val()};
			break;
		case "timestamp":
			var $d = $("input", this).val();
			if(!$d)
				$d = "";
			if(!re_js_DATE.test($d)) {
				errorText("Error: Date format error, expected 'YYYY-mm-dd HH:MM[:SS]' format");
				$("input", this).focus();
				$err = true;
				return;
			}
			$dat["content"][$cnt++] = {"timestamp": $d};
			break;
		}
	});
	if($("#siglist").children().length > 0) {
		var v = $("#siglist option:selected").val();
		if(v != -1) {
			$dat['sigfp'] = v;
		}
	}
	if($err)
		return;
	$("#errorText").stop(true, true);
	if($dat.content.length <= 2) {
		if(!confirm("Warning: No content has been added.\nPlease confirm sending the message if this is your intention or press Cancel to add content."))
			return;
	}
	$("#contentTable input").prop("disabled", true);
	$("#sendForm select").prop("disabled", true);
	$("#errorText").text("Sending message... Please wait...");
	$.post("/rest/send", $dat, function(d, s, jq) {
		/* Success */
		$("#errorText").text("Message queued with identifier '" + d + "'.");
		$("#sendForm select").prop("disabled", false);
		$("#contentTable tr").remove();
		$("#severity option").eq(0).prop('selected', true);
		$("#category option").eq(0).prop('selected', true);
		$("#subcategory option").remove();
		$("#severityValue").val("-1");
		$("#categoryValue").val("-1");
		$("#subcategoryValue").val("-1");
		$("#customCategory").css("display", "none");		// Hide custom entry fields
		$("#customSubcategory").css("display", "none");
		$("#subcategory").prop("disabled", false);		// Enable subcat selection
	}).fail(function() {
		/* Failure */
		$("#contentTable input").prop("disabled", false);
		$("#sendForm select").prop("disabled", false);
		alert("Sending message failed, server was nos not responding.");
	});
}

function signatureEnable()
{
	var chk = $(this).prop("checked");
	var sl = $("#siglist");
	sl.prop("disabled", !chk);
	if(chk) {
		$.get("/rest/sigcerts", function(resp) {
			sl.append("<option selected=\"selected\" value=\"-1\">-- Select Certificate --</option>\n");
			$("certificate", resp).each(function(c) {
				/* Filter password protected, cannot yet pass it to the backend */
				if($("needpw", this).text() === "false") {
					sl.append("<optgroup label=\""+$("txt", this).text()+"\">\n"
						 +"<option value=\"" + $("fp", this).text() + "\">" + $("cn", this).text() + "</option>\n"
						 +"</optgroup>\n");
				}
			});
		});
	} else {
		$("#siglist optgroup").remove();
		$("#siglist option").remove();
	}
}

/* JQuery ready hook */
$(document).ajaxError(function(ev, jqxhr) {
	if(jqxhr.status == 200) {
console.log(jqxhr);
		jqxhr.success();
	} else {
		$("#errorText").text("Ajax request failed, status code: " + jqxhr.status + " " + jqxhr.statusText);
	}
});
$(document).ready(function() {
	fillSeverity();
	fillCategory();
	$("#severity").on("change", updateSeverity);
	$("#addContent option").eq(0).prop('selected', true);
	$("#addContent").on("change", addContent);
	$("#category").on("change", updateCategory);
	$("#subcategory").on("change", updateSubcategory);
	$("#categoryValue").on("change", checkCategory);
	$("#subcategoryValue").on("change", checkCategory);
	$("#sigen").on("change", signatureEnable);
	/* Binding to move/remove content entries */
	$("#contentTable").on("click", ".up", upClicked);
	$("#contentTable").on("click", ".down", downClicked);
	$("#contentTable").on("click", ".remove", removeClicked);
	/* Fraction content auto update */
	$("#contentTable").on("change", ".fraction", updateFraction);
	$("#contentTable").on("change", ".integer", updateInteger);
	$("#contentTable").on("change", ".cardinal", updateCardinal);
	$("#contentTable").on("change", ".uuid", updateUUID);
	/* Submit data */
	$("#submit").on("click", submitData);
	checkCategory();
});

</script>
<noscript>
 <br />
 <div>JavaScript is (unfortunately) required for message generation and submission. Please enable JavaScript for this page to continue.</div>
 <br />
</noscript>
<div class="contenttitle">Send a PingMyDroid&trade; Message</div>
<div class="contentsubtitle">One-to-many without One-too-many</div>

<form id="sendForm">
<table>
 <tr>
  <td id="severityText" style="color:red">Severity</td>
  <td>
   <select id="severity" name="severity" title="Select the message severity">
   </select>
  </td>
  <td id="severityImg"></td>
 <tr>
  <td id="categoryText">Category</td>
  <td>
   <select id="category" name="category" title="Select the message classification category">
   </select>
  </td>
  <td>
   <div id="customCategory" style="display:none"> - Custom value <input id="categoryValue" type="number" value="-1" title="Manual input the message classification category"></div>
  </td>
 </tr>
 <tr>
  <td id="subcategoryText">Sub-category</td>
  <td>
   <select id="subcategory" name="subcategory" title="Sub-classification of the category">
   </select>
  </td>
  <td>
   <div id="customSubcategory" style="display:none"> - Custom value <input id="subcategoryValue" type="number" value="-1" title="Manual input sub-classification of the category"></div>
  </td>
 </tr>
 <tr>
  <td>Force silent</td>
  <td>
   <input id="forceSilent" name="forcesilent" type="checkbox" title="Instruct the client to inform without sound"/>
  </td>
 </tr>
 <tr><td><hr /></td><td></td></tr>
 <tr>
  <td colspan="3">
   <table id="contentTable">
   </table>
  </td>
 </tr>
 <tr><td><hr /></td><td></td></tr>
</table>
<div>
<select id="addContent" title="Add content part of specified type to the message">
 <option selected="selected" value="">-- Add Content --</option>
 <option value="text">Plain text</option>
 <option value="resource">URL/URI resource</option>
 <option value="integer">Integer value</option>
 <option value="cardinal">Cardinal value</option>
 <option value="fraction">Fractional value</option>
 <option value="uuid">UUID</option>
 <option value="timestamp">Timestamp</option>
</select>
</div>
<br />
<div>
<table>
 <tr><td>Sign message</td><td><input id="sigen" type="checkbox" /></td><td>
  <select id="siglist" disabled="disabled">
  </select>
 <td></tr>
</table>
</div>
</form>
<br />
<br />
<div id="errorText" style="float:left; color:red">
</div>
<div style="float:right">
Send Message<br /><input id="submit" type="image" src="/image/pigeon.png" width="192" height="217" title="Send the message and inform the neighborhood" />
</div>
<?php
	html_bottom("");
?>
