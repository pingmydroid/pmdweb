<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("config.inc.php");
require_once("utils.inc.php");

define("PMDT_CATEGORY", 0);
define("PMDT_SUBCATEGORY", 1);
define("PMDT_TEXT", 2);
define("PMDT_RESOURCE", 3);
define("PMDT_UUID", 4);
define("PMDT_TIMESTAMP", 5);
define("PMDT_CARDINAL", 6);
define("PMDT_INTEGER", 7);
define("PMDT_FRACTION", 8);
define("PMDT_PADDING", 65531);
define("PMDT_KEY", 65532);
define("PMDT_ENCRYPTED", 65533);
define("PMDT_FINGERPRINT", 65534);
define("PMDT_SIGNATURE", 65535);

define("HISTORDER_MIN", 0);
define("HISTORDER_ID", 0);
define("HISTORDER_TS", 1);
define("HISTORDER_SEV", 2);
define("HISTORDER_UUID", 3);
define("HISTORDER_SENT", 4);
define("HISTORDER_USR", 5);
define("HISTORDER_MAX", 5);

define("USERPRIV_SEND", 0x0001);
define("USERPRIV_HIST", 0x0002);
define("USERPRIV_CFG", 0x0100);
define("USERPRIV_CFGCRT", 0x0200);
define("USERPRIV_CFGUSR", 0x0400);
$userprivs = array(
	array('val' => USERPRIV_SEND,	'name' => "USERPRIV_SEND",	'abbr' => "Send"),
	array('val' => USERPRIV_HIST,	'name' => "USERPRIV_HIST",	'abbr' => "History"),
	array('val' => USERPRIV_CFG,	'name' => "USERPRIV_CFG",	'abbr' => "Config"),
	array('val' => USERPRIV_CFGCRT,	'name' => "USERPRIV_CFGCRT",	'abbr' => "Certificates"),
	array('val' => USERPRIV_CFGUSR,	'name' => "USERPRIV_CFGUSR",	'abbr' => "Users"),
);
$userprivmask = 0;
foreach($userprivs as $v)
	$userprivmask |= $v['val'];

$donecfg = false;

class PmdSql
{
	private $db;

	function __construct() {
		global $cfg_db_dsn, $cfg_db_username, $cfg_db_password, $donecfg;
		if($cfg_db_username)
			$this->db = new PDO($cfg_db_dsn, $cfg_db_username, $cfg_db_password);
		else
			$this->db = new PDO($cfg_db_dsn);
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
		if(!$donecfg) {
			$this->setupConfig();
			$donecfg = true;
		}
	}

	private function setupConfig() {
		global $defcfg, $cfg;
		$q = "SELECT content FROM pmd_configvalue WHERE ts=(SELECT MAX(ts) FROM pmd_configvalue)";
		if(false === ($res = $this->db->query($q)))
			return false;
		if($res->rowCount() <= 0)
			return false;
		if(false === ($row = $res->fetch(PDO::FETCH_ASSOC)))
			return false;
		if(!is_array($a = unserialize($row['content'])))
			return false;
		$cfg = array_merge($defcfg, $a);
		return true;
	}

	function saveConfig() {
		global $cfg;
		$q = "INSERT INTO pmd_configvalue VALUES (DEFAULT, ?)";
		if(false === ($sth = $this->db->prepare($q)))
			return false;

		return $sth->execute(array(serialize($cfg)));
	}

	/* Return an XML forest from a query with specified root tag */
	private function getForest($q, $root) {
		$q = $this->db->query($q);
		/* Vim chokes on the < ? ? > tags and seriously impedes syntax highlighting */
		$res = "<"."?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?".">\n<$root>\n";
		while($row = $q->fetch(PDO::FETCH_NUM)) {
			$res .= $row[0] . "\n";
		}
		$res .= "</$root>\n";
		return $res;
	}

	/* Return all categories as XML */
	function getCategories() {
		return $this->getForest("SELECT xmlelement(name category, xmlattributes(id), xmlforest(id, name, description)) FROM pmd_category ORDER BY id", "categories");
	}

	/* Return all sub-categories for a specified category or all categories as XML */
	function getSubCategories($cat = null) {
		if(is_null($cat))
			return $this->getForest("SELECT xmlelement(name subcategory, xmlattributes(id, cat), xmlforest(cat AS category, id, name, description)) FROM pmd_subcategory ORDER BY cat, id", "subcategories");
		else {
			$cs = sprintf("%d", $cat);
			return $this->getForest("SELECT xmlelement(name subcategory, xmlforest(cat AS category, id, name, description)) FROM pmd_subcategory WHERE cat = $cs ORDER BY id", "subcategories");
		}
	}

	/* Return all severities as XML */
	function getSeverities() {
		return $this->getForest("SELECT xmlelement(name severity, xmlforest(id, name, description)) FROM pmd_severity ORDER BY id DESC", "severities");
	}

	/* Return all PMD types as XML */
	function getTypes() {
		return $this->getForest("SELECT xmlelement(name type, xmlforest(id, name, description)) FROM pmd_type", "types");
	}

	/* Return the last 100 historic messages as XML */
	function getHistory($order = HISTORDER_ID) {
		if($order < HISTORDER_MIN || $order > HISTORDER_MAX)
			$order = HISTORDER_ID;
		$srt = "";
		switch($order) {
		case HISTORDER_ID:	$srt = "ORDER BY h.id DESC"; break;
		case HISTORDER_TS:	$srt = "ORDER BY h.ts DESC, h.id DESC"; break;
		case HISTORDER_SEV:	$srt = "ORDER BY h.severity ASC, h.ts ASC, h.id DESC"; break;
		case HISTORDER_UUID:	$srt = "ORDER BY h.uniid ASC, h.id DESC"; break;
		case HISTORDER_SENT:	$srt = "ORDER BY h.sent DESC, h.id DESC"; break;
		case HISTORDER_USR:	$srt = "ORDER BY h.usr ASC, h.id DESC"; break;
		}
		return $this->getForest("SELECT xmlelement(name msg, xmlforest(h.id, h.usr, to_char(h.ts, 'YYYY-MM-DD HH24:MI:SS') AS ts, s.id AS severityid, s.description AS severity, h.forcesilent, h.uniid, h.sigfp, to_char(h.sent, 'YYYY-MM-DD HH24:MI:SS') AS sent)) FROM pmd_history h JOIN pmd_severity s ON h.severity=s.id $srt LIMIT 100", "history");
	}

	/* Return the content of the spefied message as XML */
	function getHistoryMessage($id) {
		$id = sprintf("%d", $id);
		return $this->getForest("SELECT xmlelement(name content, xmlforest(m.id, m.seq, m.typ, t.description, m.u_int1, m.u_int2, m.u_ts, m.u_txt)) FROM pmd_msgcontent m JOIN pmd_type t ON m.typ=t.id WHERE m.id='$id' ORDER BY m.seq", "message");
	}

	/* Queue a message for sending */
	function queueMessage($content, $uuid) {
		if(!$this->db->beginTransaction())
			return "Failed to begin transaction";

		$q = "INSERT INTO pmd_history VALUES (DEFAULT, ?, now(), ?, ?, ?, ?, DEFAULT) RETURNING id";
		if(false === ($sth = $this->db->prepare($q))) {
			$this->db->rollBack();
			return "Failed to prepare insert history";
		}
		$sigfp = isset($content['sigfp']) ? $content['sigfp'] : null;
		if(false === $sth->execute(array($_SESSION['uid'], $content['severity'], $content['forcesilent'] ? 'true' : 'false', $uuid, $sigfp))) {
			$this->db->rollBack();
			return "Failed to insert history";
		}
		if(false === ($id = $sth->fetchColumn(0))) {
			$this->db->rollBack();
			return "Failed to fetch id";
		}

		$q = "INSERT INTO pmd_msgcontent VALUES (?, ?, ?, ?, ?, ?, ?)";
		if(false === ($sth = $this->db->prepare($q))) {
			$this->db->rollBack();
			return "Failed to prepare insert msgcontent";
		}
		$seq = 0;
		foreach($content['content'] as $c) {
			switch(key($c)) {
			case 'category':
				if(!is_numeric($c['category'])) {
					$this->db->rollBack();
					return "Invalid category, not numeric";
				}
				$vals = array($id, $seq++, PMDT_CATEGORY, $c['category'], null, null, null);
				break;
			case 'subcategory':
				if(!is_numeric($c['subcategory'])) {
					$this->db->rollBack();
					return "Invalid subcategory, not numeric";
				}
				$vals = array($id, $seq++, PMDT_SUBCATEGORY, $c['subcategory'], null, null, null);
				break;
			case 'integer':
				if(!is_numeric($c['integer'])) {
					$this->db->rollBack();
					return "Invalid integer, not numeric";
				}
				$vals = array($id, $seq++, PMDT_INTEGER, $c['integer'], null, null, null);
				break;
			case 'cardinal':
				if(!is_numeric($c['cardinal']) || $c['cardinal'] < 0) {
					$this->db->rollBack();
					return "Invalid cardinal, not numeric or signed";
				}
				$vals = array($id, $seq++, PMDT_CARDINAL, $c['cardinal'], null, null, null);
				break;
			case 'fraction':
				if(!is_array($c['fraction']) || !is_numeric($c['fraction'][0]) || !is_numeric($c['fraction'][1]) || $c['fraction'][1] < 0) {
					$this->db->rollBack();
					return "Invalid fraction, not numeric or divisor signed";
				}
				$vals = array($id, $seq++, PMDT_FRACTION, $c['fraction'][0], $c['fraction'][1], null, null);
				break;
			case 'text':
				$vals = array($id, $seq++, PMDT_TEXT, null, null, null, $c['text']);
				break;
			case 'resource':
				if(false === parse_url($c['resource'])) {
					$this->db->rollBack();
					return "Invalid resource, invalid URI format";
				}
				$vals = array($id, $seq++, PMDT_RESOURCE, null, null, null, $c['resource']);
				break;
			case 'uuid':
				if(!preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i', $c['uuid'])) {
					$this->db->rollBack();
					return "Invalid UUID format";
				}
				$vals = array($id, $seq++, PMDT_UUID, null, null, null, $c['uuid']);
				break;
			case 'timestamp':
				if(false === date_parse($c['timestamp'])) {
					$this->db->rollBack();
					return "Invalid Date/time";
				}
				$vals = array($id, $seq++, PMDT_TIMESTAMP, null, null, $c['timestamp'], null);
				break;
			default:
				$this->db->rollBack();
				return "Invalid msgcontent type '" . key($c) . "'";
			}
			if(false === $sth->execute($vals)) {
				$this->db->rollBack();
				return "Failed to insert msgcontent '" . key($c) . "'";
			}
		}

		if(false === $this->db->exec("INSERT INTO pmd_queue VALUES ($id, NULL, NULL)")) {
				$this->db->rollBack();
				return "Failed to insert into queue";
		}

		if(!$this->db->commit())
			return "Failed to commit transaction";
		return "";
	}

	/* Resend a message from history */
	function requeueMessage($oldid) {
		$uuid = make_uuid();
		if(!$this->db->beginTransaction())
			return false;
		$q = "INSERT INTO pmd_history (SELECT nextval('pmd_history_seq') AS id, (?)::text AS usr, ts, severity, forcesilent, (?)::uuid AS uniid, sigfp, NULL AS sent FROM pmd_history WHERE id=?)";
		if(false === ($sth = $this->db->prepare($q))) {
			$this->db->rollBack();
			return false;
		}
		if(false === $sth->execute(array($_SESSION['uid'], $uuid, $oldid))) {
			$this->db->rollBack();
			return false;
		}

		$q = "INSERT INTO pmd_msgcontent (SELECT currval('pmd_history_seq') AS id, seq, typ, u_int1, u_int2, u_ts, u_txt FROM pmd_msgcontent WHERE id=?)";
		if(false === ($sth = $this->db->prepare($q))) {
			$this->db->rollBack();
			return false;
		}
		if(false === $sth->execute(array($oldid))) {
			$this->db->rollBack();
			return false;
		}
		if(false === ($sth = $this->db->prepare("INSERT INTO pmd_queue VALUES (currval('pmd_history_seq'), NULL, NULL)"))) {
				$this->db->rollBack();
				return false;
		}
		if(false === $sth->execute(array())) {
			$this->db->rollBack();
			return false;
		}
		if(!$this->db->commit())
			return false;
		return true;
	}

	/* Get and PID-mark the oldest 5 messages from the queue which have not yet been sent */
	function getQueue() {
		$pid = sprintf("%d", getmypid());
		$q = "UPDATE pmd_queue SET ts=now(), pid='$pid' WHERE id IN (SELECT id FROM pmd_queue WHERE PID IS NULL ORDER BY id LIMIT 5)";
		if(false === $this->db->exec($q)) {
			return false;
		}
		$q = "SELECT id FROM pmd_queue WHERE pid='$pid'";
		if(false === ($res = $this->db->query($q))) {
			$this->releaseQueue();
			return false;
		}
		if(false === ($a = $res->fetchAll(PDO::FETCH_COLUMN, 0))) {
			$this->releaseQueue();
			return false;
		}
		return $a;
	}

	/* Release the PID-mark from the queue */
	function releaseQueue() {
		$pid = sprintf("%d", getmypid());
		$this->db->exec("UPDATE pmd_queue SET ts=NULL, pid=NULL WHERE pid='$pid'");
	}

	/* Remove an item from the queue and mark it as sent in the history */
	function deleteFromQueue($id) {
		$pid = sprintf("%d", getmypid());
		$id = sprintf("%d", $id);
		$this->db->exec("UPDATE pmd_history SET sent=now() WHERE id='$id'");
		$this->db->exec("DELETE FROM pmd_queue WHERE id='$id' AND pid='$pid'");
	}

	/* Return a message by id */
	function getMessage($id) {
		$id = sprintf("%d", $id);
		$q = "SELECT h.usr, h.ts, h.severity, h.forcesilent, h.uniid, h.sigfp, m.* FROM pmd_history h JOIN pmd_msgcontent m ON h.id=m.id WHERE h.id='$id' ORDER BY m.seq";
		if(false === ($res = $this->db->query($q))) {
			$this->releaseQueue();
			return array();
		}
		if(false === ($a = $res->fetchAll(PDO::FETCH_ASSOC)))
			return false;
		return $a;
	}

	/* PHP session functions */
	function sessionRead($id) {
		$sth = $this->db->prepare("SELECT dat FROM pmd_session WHERE id=?");
		$sth->execute(array($id . $_SERVER['REMOTE_ADDR']));
		if($sth->rowCount() <= 0) {
			$sth = $this->db->prepare("INSERT INTO pmd_session VALUES (?, '', now())");
			$sth->execute(array($id . $_SERVER['REMOTE_ADDR']));
			return '';
		}
		return $sth->fetchColumn(0);
	}

	function sessionWrite($id, $data) {
		$sth = $this->db->prepare("UPDATE pmd_session SET dat=?, ts=now() WHERE id=?");
		return true === $sth->execute(array($data, $id . $_SERVER['REMOTE_ADDR']));
	}

	function sessionDestroy($id) {
		$sth = $this->db->prepare("DELETE FROM pmd_session WHERE id=?");
		return true === $sth->execute(array($id . $_SERVER['REMOTE_ADDR']));
	}

	function sessionGc($deltat) {
		if($deltat <= 0)
			return false;
		$deltat = sprintf("%d seconds", $deltat);
		$sth = $this->db->prepare("DELETE FROM pmd_session WHERE ts + interval ? < now()");
		return true === $sth->execute(array($deltat));
	}

	/* Helper for authentication */
	function authUser($uid, $pw) {
		$sth = $this->db->prepare("SELECT priv FROM pmd_user WHERE id=(?)::text AND pwhash=md5(salt||((?)::text))");
		if(!$sth->execute(array($uid, $pw)))
			return false;
		return $sth->rowCount() > 0 ? $sth->fetchColumn(0) : false;
	}

	/* Return all users on the system */
	function getUsers() {
		if(has_priv(USERPRIV_CFGUSR))
			return $this->getForest("SELECT xmlelement(name user, xmlforest(id, priv)) FROM pmd_user ORDER BY id", "users");
		else {
			$q = "SELECT xmlelement(name user, xmlforest(id, priv)) FROM pmd_user WHERE id=?";
			$sth = $this->db->prepare($q);
			$sth->execute(array($_SESSION['uid']));
			/* Vim chokes on the < ? ? > tags and seriously impedes syntax highlighting */
			$res = "<"."?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?".">\n<users>\n";
			while($row = $sth->fetch(PDO::FETCH_NUM)) {
				$res .= $row[0] . "\n";
			}
			$res .= "</users>\n";
			return $res;
		}
	}

	function setUserPwd($uid, $pwd) {
		$salt = md5(uniqid("PmdWeb", true));
		$pwhash = md5($salt . $pwd);
		$sth = $this->db->prepare("UPDATE pmd_user SET salt=?, pwhash=? WHERE id=?");
		return $sth->execute(array($salt, $pwhash, $uid));
	}

	function setUserPriv($uid, $priv) {
		$sth = $this->db->prepare("UPDATE pmd_user SET priv=? WHERE id=?");
		return $sth->execute(array($priv, $uid));
	}

	function newUser($uid, $pwd, $priv) {
		$salt = md5(uniqid("PmdWeb", true));
		$pwhash = md5($salt . $pwd);
		$sth = $this->db->prepare("INSERT INTO pmd_user VALUES (?, ?, ?, ?)");
		return $sth->execute(array($uid, $salt, $pwhash, $priv));
	}

	function removeUser($uid) {
		$sth = $this->db->prepare("DELETE FROM pmd_user WHERE id=?");
		return $sth->execute(array($uid));
	}

	/* Get certificates */
	function getSigCerts() {
		return $this->getForest("SELECT xmlelement(name certificate, xmlforest(fp, txt, cn, subj, needpw, validfrom, validto)) FROM pmd_cert", "certificates");
	}

	function hasCert($fp) {
		$sth = $this->db->prepare("SELECT count(*) FROM pmd_cert WHERE fp=?");
		if(!$sth->execute(array($fp)))
			return false;
		return $sth->fetchColumn() > 0;
	}

	function getCert($fp) {
		$sth = $this->db->prepare("SELECT cert, pkey FROM pmd_cert WHERE fp=?");
		if(!$sth->execute(array($fp)))
			return false;
		return $sth->fetch(PDO::FETCH_ASSOC);
	}

	function updateCert($fp, $desc) {
		$sth = $this->db->prepare("UPDATE pmd_cert SET txt=? WHERE fp=?");
		return $sth->execute(array(htmlentities($desc), $fp));
	}

	function removeCert($fp, $desc) {
		$sth = $this->db->prepare("DELETE FROM pmd_cert WHERE fp=?");
		return $sth->execute(array($fp));
	}

	function insertCert($fp, $cn, $subj, $desc, $validfrom, $validto, $privkey, $pubcert, $needpw) {
		$sth = $this->db->prepare("INSERT INTO pmd_cert VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		date_default_timezone_set("UTC");
		$vf = strftime("%Y-%m-%dT%H:%M:%SZ", $validfrom);
		$vt = strftime("%Y-%m-%dT%H:%M:%SZ", $validto);
		return $sth->execute(array($fp, htmlentities($desc), htmlentities($cn), htmlentities($subj), $needpw ? "true" : "false", $vf, $vt, $pubcert, $privkey));
	}
}

?>
