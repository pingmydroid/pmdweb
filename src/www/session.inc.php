<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("pmdsql.inc.php");


function sessopen($savePath, $sessionName) {
}

function sessclose() {
	return true;
}

function sessread($id) {
	$sess_db = new PmdSql();
	return $sess_db->sessionRead($id);
}

function sesswrite($id, $data) {
	$sess_db = new PmdSql();
	return !($sess_db->sessionWrite($id, $data) === false);
}

function sessdestroy($id) {
	$sess_db = new PmdSql();
	$sess_db->sessionDestroy($id);
	return true;
}

function sessgc($maxlifetime) {
	$sess_db = new PmdSql();
	$sess_db->sessionGc($maxlifetime);
	return true;
}

session_set_save_handler('sessopen', 'sessclose', 'sessread', 'sesswrite', 'sessdestroy', 'sessgc');
session_name("PMDWebID");
session_start();

?>
