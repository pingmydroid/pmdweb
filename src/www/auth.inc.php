<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("session.inc.php");
require_once("header.inc.php");

function has_priv($priv) {
	return isset($_SESSION['priv']) && ($_SESSION['priv'] & $priv) === $priv;
}

/* Check the required privelege or redirect to authentication */
function require_priv($priv) {
	if(has_priv($priv))
		return;
	$_SESSION['require'] = $priv;
	do_auth();
}

/* Send the auth page prefilled with target and "old" user id */
function send_auth($target, $uid = "") {
	html_head("Authenticate");

	echo "<div class=\"contenttitle\">Please authenticate</div>\n";

	if(isset($_SESSION['uid'])) {
		echo "You do not have enough priveleges to access the function you desire.<br />\n";
		echo "Please enter credentials with sufficient priveleges to continue.<br /><br />\n";
	}
?>
<form action="/content/auth" method="post">
<table>
 <tr><td>Username</td><td><input type="text" name="username" value="<?php echo htmlentities($uid); ?>" /></td></tr>
 <tr><td>Password</td><td><input type="password" name="password" value="" /></td></tr>
 <tr><td></td><td><input type="submit" name="Submit" value="Authenticate" /></td></tr>
</table>
<input type="hidden" name="target" value="<?php echo htmlentities($target); ?>" />
</form>
<?php
	html_bottom("");
	exit;
}

/* Check database authentication for requested privelege level */
function do_auth() {
	$curpriv = isset($_SESSION['priv']) ? $_SESSION['priv'] : 0;
	$curuid  = isset($_SESSION['uid']) ? $_SESSION['uid'] : "";
	$curreq  = isset($_SESSION['require']) ? $_SESSION['require'] : -1;

	/* Already valid and enough priveleges */
	if($curuid && ($curpriv & $curreq) == $curreq)
		return;

	/* If we are missing any form input, simply redirect to authentication again */
	if(!isset($_REQUEST['username']) || !isset($_REQUEST['password']) || !isset($_REQUEST['target']))
		send_auth("/content" . $_SERVER['PATH_INFO']);

	$uid = $_REQUEST['username'];
	$pwd = $_REQUEST['password'];
	$tgt = $_REQUEST['target'];

	/* Try to authenticate the uid/pwd we got back */
	$db = new PmdSql();
	$priv = $db->authUser($uid, $pwd);
	if(false === $priv)
		send_auth($tgt, $uid);

	/* We are a user of the system, do we have priveleges enough? */
	if(($priv & $curreq) != $curreq)
		send_auth($tgt, $uid);

	/* A valid user with priveleges */
	$_SESSION['uid'] = $uid;
	$_SESSION['priv'] = $priv;
}


?>
