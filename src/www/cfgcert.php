<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
	require_once("auth.inc.php");

	require_priv(USERPRIV_CFGCRT);

	html_head("Configure PingMyDroid&trade; Certificates");

?>
<script>
function imgX() {
	return '<input type="image" class="remove" alt="Remove Certificate" title="Remove Certificate" width="23" height="23" src="/image/tasto-x-int.png" />';
}

function imgOk() {
	return '<input type="image" class="update" alt="Update Description" title="Update Description" width="23" height="23" src="/image/tasto-ok-int.png" />';
}

function imgDl() {
	return '<input type="image" class="dlcert" alt="Download Certificate" title="Download Certificate" width="23" height="23" src="/image/certificate.png" />';
}

function imgKey() {
	return '<input type="image" class="dlkey" alt="Download Private Key and Certificate" title="Download Private Key and Certificate" width="47" height="23" src="/image/key.png" />';
}

function fillCertificates() {
	$("#certlist tr").remove();
	$("#certlist").append("<tr><th>Description</th><th>commonName</th><th></th></tr>\n");
	$.get("/rest/sigcerts", function(resp) {
		$("certificate", resp).each(function(c) {
			var tr = "<tr>"
				+ "<td class=\"dropdown desc\">" + $("txt", this).text() + "</td>"
				+ "<td class=\"dropdown cn\">" + $("cn", this).text() + "</td>"
				+ "<td>" + imgX() + "</td>"
				+ "<td><a href=\"/rest/getcert?fp="+$("fp", this).text()+"&key=false\">" + imgDl() + "</a></td>"
				+ "<td><a href=\"/rest/getcert?fp="+$("fp", this).text()+"&key=true\">" + imgKey() + "</a></td>"
				+ "</tr>\n";
			$("#certlist").append(tr);
			var tr = "<tr style=\"display:none\"><td colspan=\"5\"><table>"
				+ "<tr><td>Description</td><td><input type=\"text\" size=\"64\" value=\"" + $("txt", this).text() + "\"/>&nbsp;"+imgOk()+"</td></tr>\n"
				+ "<tr><td>Fingerprint</td><td class=\"fingerprint\">" + $("fp", this).text() + "</td></tr>\n"
				+ "<tr><td>Subject</td><td>" + $("subj", this).text() + "</td></tr>\n"
				+ "<tr><td>Valid from</td><td>" + $("validfrom", this).text() + "</td></tr>\n"
				+ "<tr><td>Valid to</td><td>" + $("validto", this).text() + "</td></tr>\n"
				+ "<tr><td>Need password</td><td>" + $("needpw", this).text() + "</td></tr>\n"
				+ "</table></td></tr>\n";
			$("#certlist").append(tr);
		});
	});
}

function certClicked() {
	var trid = $(this).parent().children().eq(0).text();
	var tr = $(this).parent().next();
	if(tr.css("display") === "none") {
		tr.css("display", "visible");
	} else {
		tr.css("display", "none");
	}
}
function genCertEnable()
{
	var tc = $("#gencert");
	if(tc.css("display") == "none") {
		tc.css("display", "visible");
		$("#gencertenable").val("Hide Certificate Import/Generation")
	} else {
		tc.css("display", "none");
		$("#gencertenable").val("Import/Generate a Certificate")
	}
}

function genCertButton() {
	errorInput("");
	var i = $("#gencert :input");
	var c = $.trim(i.filter('[name="country"]').val()).toUpperCase();
	i.filter('[name="country"]').val(c);
	var st = $.trim(i.filter('[name="state"]').val());
	var l = $.trim(i.filter('[name="locality"]').val());
	var o = $.trim(i.filter('[name="organization"]').val());
	var ou = $.trim(i.filter('[name="unit"]').val());
	var ema = $.trim(i.filter('[name="email"]').val());
	var cn = $.trim(i.filter('[name="cn"]').val());
	var pwd = $.trim(i.filter('[name="pwd"]').val());
	var desc = $.trim(i.filter('[name="desc"]').val());
	var valid = $.trim(i.filter('[name="validity"]').val());
	var vunit = $("option:selected", i.filter('[name="validunits"]')).val();
	if(!/^[A-Z]{2}$/.test(c)) {
		errorInput("Country must be 2 letter abbreviation");
		return;
	}
	if(st.length <= 0) {
		errorInput("StateOrProvince must not be empty");
		return;
	}
	if(o.length <= 0) {
		errorInput("organizationName must not be empty");
		return;
	}
	if(ema.length <= 0) {
		errorInput("emailAddress must not be empty");
		return;
	}
	if(valid < 1 || valid*vunit < 1) {
		errorInput("Certificate Validity must be > 0");
		return;
	}
	var dat = {
		"c"	: c,
		"st"	: st,
		"l"	: l,
		"o"	: o,
		"ou"	: ou,
		"email"	: ema,
		"cn"	: cn,
		"pwd"	: pwd,
		"desc"	: desc,
		"valid"	: valid*vunit
	};
	i.prop("disabled", true);
	$.post("/rest/generatecert", dat, function(d, s, jq) {
		i.prop("disabled", false);
		fillCertificates();
		errorInput("Certificate generated");
	}).fail(function() {
		/* Failure */
		i.prop("disabled", false);
		errorInput("Cerificate generation error");
	});
}

function errorInput(s) {
	$("#errorInputText").text(s);
}

function updateClicked() {
	var dat = {
		"fp"	: $(".fingerprint", $(this).parentsUntil("table").next()).text(),
		"desc"	: $(this).prev().val()
	};
	errorInput("Updating...");
	$.get("/rest/updatecert", dat, function(d, s, jq) {
		fillCertificates();
		errorInput("Certificate updated");
	}).fail(function() {
		/* Failure */
		errorInput("Cerificate update error");
	});
}

function removeClicked() {
	var fp = $(".fingerprint", $(this).parentsUntil("table").next()).text();
	var cn = $(this).parent().prev().text();
	var desc = $(this).parent().prev().prev().text();
	if(confirm("You are about to remove certificate '"+desc+"' permanently.\nfingerprint="+fp+"\ncommonName="+cn+"\nAre you sure?")) {
		var dat = { "fp" : fp };
		$.get("/rest/removecert", dat, function(d, s, jq) {
			fillCertificates();
			errorInput("Certificate removed");
		}).fail(function() {
			/* Failure */
			errorInput("Cerificate remove error");
		});
	}
}

$(document).ready(function() {
	fillCertificates();
	$("#gencertenable").on("click", genCertEnable);
	$("#gencertbutton").on("click", genCertButton);
	$("#certlist").on("mouseover", ".dropdown", function() {
		$(this).css("text-decoration", "underline");
	});
	$("#certlist").on("mouseleave", ".dropdown", function() {
		$(this).css("text-decoration", "none");
	});
	$("#certlist").on("click", ".dropdown", certClicked);
	$("#certlist").on("click", ".update", updateClicked);
	$("#certlist").on("click", ".remove", removeClicked);
});

</script>
<noscript>
 <br />
 <div>JavaScript is (unfortunately) required for message generation and submission. Please enable JavaScript for this page to continue.</div>
 <br />
</noscript>
<div class="contenttitle">Configure PingMyDroid&trade; Certificates</div>
<table id="certlist">
</table>
<hr />
<br />
<input id="gencertenable" type="button" value="Import/Generate Certificate" />
<table id="gencert" style="display:none">
 <tr><td>countryName (C)</td><td><input type="text" name="country" title="Country Name (2 letter code)" /></td></tr>
 <tr><td>stateOrProvinceName (ST)</td><td><input type="text" name="state" title="State or Province Name (full name)" /></td></tr>
 <tr><td>localityName (L)</td><td><input type="text" name="locality" title="Locality Name (eg, city)" /></td></tr>
 <tr><td>organizationName (O)</td><td><input type="text" name="organization" title="Organization Name (eg, company)" /></td></tr>
 <tr><td>organizationalUnitName (OU)</td><td><input type="text" name="unit" title="Organizational Unit Name (eg, section)" /></td></tr>
 <tr><td>emailAddress</td><td><input type="text" name="email" title="Email Address" /></td></tr>
 <tr><td>commonName (CN)</td><td><input type="text" name="cn" title="Common Name (eg, your name or your server's hostname)" /></td></tr>
 <tr><td>challengePassword</td><td><input type="password" name="pwd" title="A challenge password to protect the private key" /></td></tr>
 <tr><td>Certificate Validity</td><td><input type="number" name="validity" value="1" title="Valid time of certificate" />
   <select name="validunits">
    <option value="1">Day(s)</option>
    <option value="7">Week(s)</option>
    <option value="30">Month(s)</option>
    <option selected="selected" value="365">Year(s)</option>
   </select></td>
 </tr>
 <tr><td>Description</td><td><input type="text" name="desc" title="Certificate description for internal use" /></td></tr>
 <tr><td colspan="2">
  <form enctype="multipart/form-data" method="post" action="/rest/importcert">
   <table>
    <tr><td></td><td><input id="gencertbutton" type="button" value="Generate Certificate" /></td></tr>
     <tr><td>&nbsp;</td></tr>
     <tr><td>Import certificate</td><td><input type="file" name="certfile" value="" title="Filename of certificate + private key to import (PEM format)" />
     <tr><td>Description</td><td><input type="text" name="desc" title="Certificate description for internal use" /></td></tr>
     <tr><td></td><td><input id="importcertbutton" type="submit" value="Import Certificate" title="Send certificate file for import to server" /></td></tr>
   </table>
  </form></td></tr>
</table>
<br />
<div id="errorInputText" style="float:left; color:red"><?php echo isset($_REQUEST['upload']) ? htmlentities($_REQUEST['upload']) : ""; ?></div>
<?php
	html_bottom("");
?>
