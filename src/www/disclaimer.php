<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
	require_once("header.inc.php");
	html_head("Disclaimer - PingMyDroid&trade;");
?>
<div class="contenttitle">Disclaimer</div>
<div class="contentsubtitle">It is not my fault!</div>
You, the reader or user of the provided information, have
<i><u>implicitly</u></i> agreed to this disclaimer by entering this site or
using its information and accept this disclaimer to govern your use of any and
all information from or derived from this site.<br />
<ul>
 <li>You are solely responsible for your own fate and agree to not to blame me
or my affiliates for your unfortune in whatever form.</li>
 <li>You agree to not to take offence about any information provided by or
derived from this site. Your only recourse is to leave me and my affiliates
alone and not to visit, read, remember, see and use any information provided by
or derived from this site.</li>
 <li>Upon request you accept to provide a <i>copy</i> of your soul for
inspection (for those jurisdictions that adapt to that dogma of beings with
souls). Any copies will be destroyed after inspection. You are solely
responsible for your soul and copy/copies of your soul. You agree not to blame
anyone else but yourself for what happens to your soul or copy/copies of your
soul.</li>
</ul>
<?php
	html_bottom("");
?>
