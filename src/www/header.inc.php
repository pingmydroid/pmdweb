<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("utils.inc.php");
require_once("session.inc.php");

$global_menu = array(
	"/content"		=>	"Home",
	"",
	"/content/send"		=>	"Send&nbsp;Message",
	"/content/history"	=>	"Message&nbsp;History",
	"",
	"/content/config"	=>	"Configuration",
	"sub1"			=>	array(
		"/content/cfgcert"	=> "Certificates",
		"/content/cfguser"	=> "Users",
	),
/*
	"sub1"			=>	array(
		"/xxx"	=> "xxx",
		"/yyy"	=> "yyy",
		),
*/
	"",
	"/content/about"	=>	"About",
	"/content/disclaimer"	=>	"Disclaimer",
	"/content/trademark"	=>	"Trademarks",
	);

function make_menu()
{
	global $global_menu;
	echo " <ul class=\"menulist\">\n";
	foreach($global_menu as $p => $n) {
		if(is_array($n)) {
			echo "  <li><ul class=\"menulist2\">\n";
			foreach($n as $pp => $nn) {
				if(is_string($pp))
					echo "   <li class=\"menuitem2\"><a href=\"$pp\">$nn</a></li>\n";
				else
					echo "   <li class=\"menuitem2\">&nbsp;</li>\n";
			}
			echo "  </ul></li>\n";
		} else {
			if(is_string($p))
				echo "  <li class=\"menuitem\"><a href=\"$p\">$n</a></li>\n";
			else
				echo "  <li class=\"menuitem\">&nbsp;</li>\n";
		}
	}
	if(isset($_SESSION['uid'])) {
		echo "  <li class=\"menuitem\">&nbsp;</li>\n";
		echo "  <li class=\"menuitem\"><a href=\"/content/logout\">Logout ".htmlentities($_SESSION['uid'])."</a></li>\n";
	}
	echo " </ul>\n";
}

function html_head($title, $hdr = "")
{
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
 <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
 <link rel="stylesheet" type="text/css" media="all" href="/css/pingmydroid.css" />
 <title><?php echo $title; ?></title>
 <script src="/js/jquery-1.11.0.min.js"></script>
 <?php echo $hdr; ?>
</head>
<body>
<table class="hdrtable">
 <tr><td class="hdrname">PingMyDroid&trade;</td><td class="hdrlogo"><a href="http://www.pingmydroid.org"><img class="hdrimg" alt="PingMyDroid&trade; Logo" src="/image/pingmydroid.logo.png" /></a></td></tr>
 <tr><td class="hdrline" colspan="2"></td></tr>
 <tr><td colspan="2">&nbsp;</td></tr>
</table>
<table id="container">
<tr class="container row">
<td class="menu column">
<?php
	make_menu();
?>
</td>
<td class="content column">
<?php
}

function html_bottom($ftr = "")
{
	global $pmdweb_version;
?>
</td>
<td class="spacer column">&nbsp;</td>
</tr>
</table>
<table class="hdrtable">
 <tr><td colspan="3">&nbsp;</td></tr>
 <tr><td class="hdrline" colspan="3"></td></tr>
 <tr><td class="hdrempty">
<a rel="license" href="https://www.gnu.org/licenses/agpl-3.0.html"><img alt="Affero General Public License 3" style="border-width:0" width="88" height="33" src="/image/agplv3-88x31.png" /></a> &copy;&nbsp;2014&nbsp;<a href="http://www.vagrearg.org">Vagrearg</a></td><td>PmdWeb v<?php echo $pmdweb_version; ?></td><td class="hdrlogo">Informing my Neighborhood</td></tr>
</table>
</body>
</html>
<?php
	session_write_close();
}

?>
