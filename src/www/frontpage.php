<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
	require_once("header.inc.php");
	html_head("PingMyDroid&trade;");
?>
<div class="contenttitle">Multicast Event Distribution</div>
<div class="contentsubtitle">Informing differently</div>
Imagine a world of communication. You don't have to, you are already in one.<br />
<br />
You are communicating electronically on a daily basis, but are the initiator of
the communication. How can you be informed of events without being an
initiator?<br />
Enter the realm of PingMyDroid&trade; and the already existing multicast
network. Use your smartphone, personal computer or any other device as a
receiver for local and global events. You are the master, you select what you
want to hear about. You don't need to initiate the communication, just be
prepared to listen.<br />
<br />
All PingMyDroid&trade; communication can be signed for sender identification
and verification. You choose what information you want to receive at your
terms. PingMyDroid&trade; communication can be encrypted for transmitting
events to a select group of trusted recipients. No eavesdropping on your
critical infrastructure.
<?php
	html_bottom("");
?>
