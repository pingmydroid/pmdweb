<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
	require_once("www/config.inc.php");
	require_once("www/auth.inc.php");
	require_once("www/utils.inc.php");

function queuerunner()
{
	$qr = dirname(__FILE__) . '/queuerunner';
	if(!is_executable($qr))
		return;
	@system("$qr < /dev/null > /dev/null 2>&1 &");
}

function badrequest($s) {
		header("HTTP/1.0 400 Bad Request");
		echo "$s\n";
		exit;
}

function unauthorized() {
		header("HTTP/1.0 403 Forbidden");
		echo "You have no access rights to perform this operation.\n";
		exit;
}

function redirectcert($s) {
	@unlink($_FILES['certfile']['tmp_name']);
	header("Location: /content/cfgcert?upload=" . urlencode($s));
	exit;
}

function check_boolean($b) {
	if($_REQUEST[$b] === "true" || $_REQUEST[$b] === 1)
		$_REQUEST[$b] = true;
	else if($_REQUEST[$b] === "false" || $_REQUEST[$b] === 0)
		$_REQUEST[$b] = false;
	if(!is_bool($_REQUEST[$b]))
		badrequest("Field '$b' is not boolean");
}

function check_integer($b, $mini, $maxi) {
	if(!is_numeric($_REQUEST[$b]) || $_REQUEST[$b] < $mini || $_REQUEST[$b] > $maxi)
		badrequest("Field '$b' is not integer or out of bounds");
}

function check_targets() {
	$re_host_port = '/^(?:\[(?:(?:(?:(?:[0-9A-Fa-f]{1,4}:){6}|::(?:[0-9A-Fa-f]{1,4}:){5}|(?:[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){4}|(?:(?:[0-9A-Fa-f]{1,4}:){0,1}[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){3}|(?:(?:[0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){2}|(?:(?:[0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})?::[0-9A-Fa-f]{1,4}:|(?:(?:[0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})?::)(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))|(?:(?:[0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})?::[0-9A-Fa-f]{1,4}|(?:(?:[0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})?::)|[Vv][0-9A-Fa-f]+\.[A-Za-z0-9\-._~!$&\'()*+,;=:]+)\]|(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)|(?:[A-Za-z0-9\-._~!$&\'()*+,;=]|%[0-9A-Fa-f]{2})*):\d{1,5}$/';

	foreach($_REQUEST['targets'] as $h) {
		if(!preg_match($re_host_port, $h))
			badrequest("Invalid target '$h'");
	}
}

function check_domains() {
	$re_domain = '/^(([0-9a-z_][a-z0-9-]*)?[0-9a-z]\.)+[a-z]{2,}\.$/i';
	foreach($_REQUEST['domains'] as $d) {
		if(!preg_match($re_domain, $d))
			badrequest("Invalid domain '$d'");
	}
}

	$ruri = $_SERVER['PATH_INFO'];
	$m = explode('/', trim($ruri));
	switch($m[1]) {
	case 'categories':
		$db = new PmdSql();
		header("Content-Type: text/xml");
		echo $db->getCategories();
		break;
	case 'subcategories':
		$db = new PmdSql();
		header("Content-Type: text/xml");
		if(isset($_REQUEST['category'])) {
			$n = is_numeric($_REQUEST['category']) ? $_REQUEST['category'] : null;
		} else
			$n = null;
		echo $db->getSubCategories($n);
		break;
	case 'severities':
		$db = new PmdSql();
		header("Content-Type: text/xml");
		echo $db->getSeverities();
		break;
	case 'types':
		$db = new PmdSql();
		header("Content-Type: text/xml");
		echo $db->getTypes();
		break;
	case 'history':
		if(!has_priv(USERPRIV_HIST))
			unauthorized();
		$order = isset($_REQUEST['order']) ? (int)$_REQUEST['order'] : HISTORDER_ID;
		$db = new PmdSql();
		header("Content-Type: text/xml");
		echo $db->getHistory($order);
		break;
	case 'historymessage':
		if(!has_priv(USERPRIV_HIST))
			unauthorized();
		$db = new PmdSql();
		header("Content-Type: text/xml");
		if(isset($_REQUEST['msgid'])) {
			$n = is_numeric($_REQUEST['msgid']) ? $_REQUEST['msgid'] : -1;
		} else
			$n = -1;
		echo $db->getHistoryMessage($n);
		break;
	case 'send':
		if(!has_priv(USERPRIV_SEND))
			unauthorized();
		if(!isset($_REQUEST['severity']))
			badrequest("Missing severity");
		if(!is_numeric($_REQUEST['severity']))
			badrequest("Severity not numeric");
		if(!isset($_REQUEST['forcesilent']))
			badrequest("Missing forcesilent");
		if($_REQUEST['forcesilent'] !== "false" && $_REQUEST['forcesilent'] !== "true")
			badrequest("Forcesilent not a boolean");
		$_REQUEST['forcesilent'] = $_REQUEST['forcesilent'] === "true";
		if(!isset($_REQUEST['content']))
			$_REQUEST['content'] = array();		// Need an empty array if none passed
		if(!is_array($_REQUEST['content']))
			badrequest("Content not an array");
		$uuid = make_uuid();
		$db = new PmdSql();
		$s = $db->queueMessage($_REQUEST, $uuid);
		if($s !== "")
			badrequest($s);

		/* Spawn a queue runner in the background to send the message */
		queuerunner();
		header("Content-Type: text/plain");
		echo $uuid;
		break;
	case 'resend':
		if(!has_priv(USERPRIV_SEND))
			unauthorized();
		if(!isset($_REQUEST['id']))
			badrequest("Missing id");
		$db = new PmdSql();
		if(!($uuid = $db->requeueMessage($_REQUEST['id'])))
			badrequest("Failed to requeue message");

		queuerunner();
		
		header("Content-Type: text/plain");
		echo $uuid;
		break;
	case 'setconfig':
		if(!has_priv(USERPRIV_CFG))
			unauthorized();
		foreach($cfg as $k => $v) {
			if(!is_array($v) && !isset($_REQUEST[$k]))
				badrequest("Missing field '$k'");
			if(is_array($v) && isset($_REQUEST[$k]) && !is_array($_REQUEST[$k]))
				badrequest("Wrong type for field '$k'");
		}
		check_boolean('ipv4');
		check_boolean('ipv6');
		check_boolean('nolinklocal');
		check_boolean('nodns');
		check_boolean('defaddr');
		check_integer('repeat', 1, 16);
		check_integer('repeatinterval', 0, 10000);
		$newcfg = array(
			'ipv4'			=> $_REQUEST['ipv4'],
			'ipv6'			=> $_REQUEST['ipv6'],
			'nolinklocal'		=> $_REQUEST['nolinklocal'],
			'nodns'			=> $_REQUEST['nodns'],
			'defaddr'		=> $_REQUEST['defaddr'],
			'repeat'		=> $_REQUEST['repeat'],
			'repeatinterval'	=> $_REQUEST['repeatinterval'],
			'sources'		=> array(),
			'targets'		=> array(),
			'domains'		=> array()
		);
		if(isset($_REQUEST['sources']))
			$newcfg['sources'] = $_REQUEST['sources'];
		if(isset($_REQUEST['targets'])) {
			check_targets();
			$newcfg['targets'] = $_REQUEST['targets'];
		}
		if(isset($_REQUEST['domains'])) {
			check_domains();
			$newcfg['domains'] = $_REQUEST['domains'];
		}
		if($cfg == $newcfg) {
			header("Content-Type: text/plain");
			echo "Config not altered, no update required\n";
			break;
		}
		$db = new PmdSql();
		$cfg = $newcfg;
		if(!$db->saveConfig())
			badrequest("Configuration update failed.");
		header("Content-Type: text/plain");
		echo "Config Updated\n";
		break;
	case 'sigcerts':
		if(!has_priv(USERPRIV_SEND))
			unauthorized();
		$db = new PmdSql();
		header("Content-Type: text/xml");
		echo $db->getSigCerts();
		break;
	case 'generatecert':
		if(!has_priv(USERPRIV_CFGCRT))
			unauthorized();
		$dn = array();
		if(!isset($_REQUEST['c']) || !preg_match('/^[A-Z]{2}$/', $_REQUEST['c']))
			badrequest("Country (c) must be 2 letter abbreviation");
		if(!isset($_REQUEST['st']))
			badrequest("StateOrProvinceName (st) must be defined");
		if(!isset($_REQUEST['l']))
			badrequest("LocalityName (l) must be defined");
		if(!isset($_REQUEST['o']))
			badrequest("Organization (o) must be defined");
		if(!isset($_REQUEST['cn']))
			badrequest("CommonName (cn) must be defined");
		if(!isset($_REQUEST['email']))
			badrequest("EmailAddress (email) must be defined");
		$ou = isset($_REQUEST['ou']) ? $_REQUEST['ou'] : "";
		if(!isset($_REQUEST['valid']) || !is_numeric($_REQUEST['valid']) || $_REQUEST['valid'] < 1 || $_REQUEST['valid'] > 50*365)
			badrequest("Validity must be defined (in days) and be >= 1 and <= 50*365");
		$valid = (int)$_REQUEST['valid'];
		$pwd = isset($_REQUEST['pwd']) ? $_REQUEST['pwd'] : false;
		$desc = isset($_REQUEST['desc']) ? $_REQUEST['desc'] : "";
		$dn['countryName'] = $_REQUEST['c'];
		$dn['stateOrProvinceName'] = $_REQUEST['st'];
		$dn['localityName'] = $_REQUEST['l'];
		$dn['organizationName'] = $_REQUEST['o'];
		$cn = $dn['commonName'] = $_REQUEST['cn'];
		$dn['emailAddress'] = $_REQUEST['email'];
		if($ou != "")
			$dn['organizationalUnitName'] = $ou;
		$pkcfg = array(
			"digest_alg" => "sha1",
			"private_key_bits" => 2048,
			"private_key_type" => OPENSSL_KEYTYPE_RSA,
			'encrypt_key' => $pwd !== false,
		);
		$req_key = openssl_pkey_new();
		$req_csr = openssl_csr_new($dn, $req_key);
		if(false === ($cert = openssl_csr_sign($req_csr, NULL, $req_key, $valid)))
			badrequest("Certificate could not be self-signed");

		if($pwd)
			openssl_pkey_export($req_key, $privkey, $pwd);
		else
			openssl_pkey_export($req_key, $privkey);
		openssl_x509_export($cert, $pubcert);
		$certdata = openssl_x509_parse($cert);
		$subj = "";
		foreach($certdata['subject'] as $k => $v) {
			if($k == 'CN' || $k == 'emailAddress')
				continue;
			$subj .= "$k=$v, ";
		}
		$subj .= "CN=".$certdata['subject']['CN']."/".$certdata['subject']['emailAddress'];

		/* openssl_x509_fingerprint() is only available from PHP 5.6 */
		$cb = strpos($pem, '-----BEGIN CERTIFICATE-----');
		$ce = strpos($pem, '-----END CERTIFICATE-----');
		if(false === $cb || false === $ce || $cb >= $ce)
			badrequest("Certificate missing tags");
		$fp = sha1(base64_decode(substr($pubcert, $cb+27, $ce-$cb-27)));

		$validfrom = $certdata['validFrom_time_t'];
		$validto = $certdata['validTo_time_t'];
		$db = new PmdSql();
		if($db->insertCert($fp, $cn, $subj, $desc, $validfrom, $validto, $privkey, $pubcert, $pwd)) {
			header("Content-Type: text/plain");
			//print_r($certdata);
			echo $fp;
		} else
			badrequest("Certificate could not be inserted into database");
		break;
	case 'importcert':
		if(!has_priv(USERPRIV_CFGCRT))
			unauthorized();
		$desc = isset($_REQUEST['desc']) ? $_REQUEST['desc'] : "";
		if(!isset($_FILES['certfile']))
			redirectcert("Missing certificate file");
		if(UPLOAD_ERR_OK !== $_FILES['certfile']['error'])
			redirectcert("Upload error");
		if(!is_uploaded_file($_FILES['certfile']['tmp_name']))
			redirectcert("Upload error");
		if($_FILES['certfile']['size'] > 65535)		// Certificates are not big, really
			redirectcert("Upload file too large");
		$pem = file_get_contents($_FILES['certfile']['tmp_name']);
		$cb = strpos($pem, '-----BEGIN CERTIFICATE-----');
		$ce = strpos($pem, '-----END CERTIFICATE-----');
		if(false === $cb || false === $ce || $cb >= $ce)
			redirectcert("Missing certificate in file");
		$crt = substr($pem, $cb, $ce - $cb + 25) . "\n";
		$kb = strpos($pem, '-----BEGIN PRIVATE KEY-----');
		$ke = strpos($pem, '-----END PRIVATE KEY-----');
		$keb = strpos($pem, '-----BEGIN ENCRYPTED PRIVATE KEY-----');
		$kee = strpos($pem, '-----END ENCRYPTED PRIVATE KEY-----');
		if(false !== $kb) {
			if(false === $ke || $kb >= $ke)
				redirectcert("Missing private key in file");
			$key = substr($pem, $kb, $ke - $kb + 25) . "\n";
			$pwd = false;
		} else if(false !== $keb) {
			if(false === $kee || $keb >= $kee)
				redirectcert("Missing encrypted private key in file");
			$key = substr($pem, $keb, $kee - $keb + 35) . "\n";
			$pwd = true;
		} else
			redirectcert("Missing private key in file");
		if(false === ($cert = openssl_x509_read($crt)))
			redirectcert("Certificate invalid");
		$certdata = openssl_x509_parse($cert);
		$subj = "";
		foreach($certdata['subject'] as $k => $v) {
			if($k == 'CN' || $k == 'emailAddress')
				continue;
			$subj .= "$k=$v, ";
		}
		if(!isset($certdata['subject']['CN']))
			$certdata['subject']['CN'] = "<unknown>";
		if(!isset($certdata['subject']['emailAddress']))
			$certdata['subject']['emailAddress'] = "<unknown>";
		$subj .= "CN=".$certdata['subject']['CN']."/".$certdata['subject']['emailAddress'];
		/* openssl_x509_fingerprint() is only available from PHP 5.6 */
		$fp = sha1(base64_decode(substr($pem, $cb+27, $ce-$cb-27)));
		$validfrom = $certdata['validFrom_time_t'];
		$validto = $certdata['validTo_time_t'];
		$cn = $certdata['subject']['CN'];
		$db = new PmdSql();
		if($db->hasCert($fp))
			redirectcert("Certificate already in database");
		if(!$db->insertCert($fp, $cn, $subj, $desc, $validfrom, $validto, $key, $crt, $pwd))
			redirectcert("Certificate could not be inserted into database");
		redirectcert("Certificate uploaded and imported");
		break;
	case 'updatecert':
		if(!has_priv(USERPRIV_CFGCRT))
			unauthorized();
		if(!isset($_REQUEST['fp']))
			badrequest("Missing fingerprint");
		if(!isset($_REQUEST['desc']))
			badrequest("Missing description");
		$db = new PmdSql();
		if(!$db->updateCert($_REQUEST['fp'], $_REQUEST['desc']))
			badrequest("Update failed");
		header("Content-Type: text/plain");
		echo "OK\n";
		break;
	case 'removecert':
		if(!has_priv(USERPRIV_CFGCRT))
			unauthorized();
		if(!isset($_REQUEST['fp']))
			badrequest("Missing fingerprint");
		$db = new PmdSql();
		if(!$db->removeCert($_REQUEST['fp']))
			badrequest("Remove failed");
		header("Content-Type: text/plain");
		echo "OK\n";
		break;
	case 'getcert':
		if(!has_priv(USERPRIV_CFGCRT))
			unauthorized();
		if(!isset($_REQUEST['fp']))
			badrequest("Missing fingerprint");
		if(!isset($_REQUEST['key']))
			badrequest("Missing key argument");
		$db = new PmdSql();
		if(false === ($cert = $db->getCert($_REQUEST['fp'])))
			badrequest("Certificate retrieval failed");
		header("Content-Type: text/plain");
		echo $cert['cert'];
		if($_REQUEST['key'] === "true")
			echo $cert['pkey'];
		break;
	case 'getusers':
		$db = new PmdSql();
		header("Content-Type: text/xml");
		echo $db->getUsers();
		break;
	case 'updatepwd':
		if(!isset($_SESSION['uid']))
			badrequest("Missing session");
		if(!isset($_REQUEST['uid']))
			badrequest("Missing user ID");
		if(!isset($_REQUEST['pwd']) || strlen($_REQUEST['pwd']) <= 0)
			badrequest("Missing password");
		if($_SESSION['uid'] !== $_REQUEST['uid'] && !has_priv(USERPRIV_CFGUSR))
			unauthorized();
		$db = new PmdSql();
		if(!$db->setUserPwd($_REQUEST['uid'], $_REQUEST['pwd']))
			badrequest("Database update error");
		header("Content-Type: text/plain");
		echo "OK\n";
		break;
	case 'updatepriv':
		if(!has_priv(USERPRIV_CFGUSR))
			unauthorized();
		if(!isset($_REQUEST['uid']))
			badrequest("Missing user ID");
		if(!isset($_REQUEST['priv']) || !is_numeric($_REQUEST['priv']))
			badrequest("Missing privelege");
		$db = new PmdSql();
		if(!$db->setUserPriv($_REQUEST['uid'], $_REQUEST['priv']))
			badrequest("Database update error");
		header("Content-Type: text/plain");
		echo "OK\n";
		break;
	case 'newuser':
		if(!has_priv(USERPRIV_CFGUSR))
			unauthorized();
		if(!isset($_REQUEST['uid']) || !preg_match('/^[a-zA-Z][a-zA-Z0-9_.-]+/', $_REQUEST['uid']))
			badrequest("Missing or invalid user ID");
		if(!isset($_REQUEST['pwd']) || strlen($_REQUEST['pwd']) <= 0)
			badrequest("Missing or invalid password");
		if(!isset($_REQUEST['priv']) || !is_numeric($_REQUEST['priv']))
			badrequest("Missing privelege");
		$db = new PmdSql();
		if(!$db->newUser($_REQUEST['uid'], $_REQUEST['pwd'], $_REQUEST['priv']))
			badrequest("Database update error");
		header("Content-Type: text/plain");
		echo "OK\n";
		break;
	case 'removeuser':
		if(!isset($_REQUEST['uid']) || $_REQUEST['uid'] == "admin")
			badrequest("Missing user ID");
		if(!(isset($_SESSION['uid']) && $_REQUEST['uid'] == $_SESSION['uid']) && !has_priv(USERPRIV_CFGUSR))
			unauthorized();
		$db = new PmdSql();
		if(!$db->removeUser($_REQUEST['uid']))
			badrequest("Database update error");
		if($_REQUEST['uid'] == $_SESSION['uid'])
			$_SESSION = array();
		header("Content-Type: text/plain");
		echo "OK\n";
		break;
	default:
		header("HTTP/1.0 404 Not Found");
		header("Content-Type: text/plain");
		echo "Rest request '".$_SERVER['PATH_INFO']."' was not located here\n";
		break;
	}
?>
