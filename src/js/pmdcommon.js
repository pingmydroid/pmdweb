/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Return the severity image as an img tag */
function severityImage(sev) {
	switch(sev) {
	case "0":	return "<img title=\"Emergency\" alt=\"Emergency\" width=\"29\" height=\"22\" src=\"/image/sev-emergency.png\" />";
	case "1":	return "<img title=\"Alert\" alt=\"Alert\" width=\"22\" height=\"22\" src=\"/image/sev-alert.png\" />";
	case "2":	return "<img title=\"Critical\" alt=\"Critical\" width=\"22\" height=\"22\" src=\"/image/sev-critical.png\" />";
	case "3":	return "<img title=\"Error\" alt=\"Error\" width=\"22\" height=\"22\" src=\"/image/sev-error.png\" />";
	case "4":	return "<img title=\"Warning\" alt=\"Warning\" width=\"25\" height=\"22\" src=\"/image/sev-warning.png\" />";
	case "5":	return "<img title=\"Notice\" alt=\"Notice\" width=\"18\" height=\"22\" src=\"/image/sev-notice.png\" />";
	case "6":	return "<img title=\"Information\" alt=\"Information\" width=\"22\" height=\"22\" src=\"/image/sev-info.png\" />";
	case "7":	return "<img title=\"Debug\" alt=\"Debug\" width=\"28\" height=\"22\" src=\"/image/sev-debug.png\" />";
	default: return "";
	}
}

