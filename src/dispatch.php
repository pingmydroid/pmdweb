<?php
/*
 * PingMyDroid(TM) Web-interface
 * Copyright (C) 2014 B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
	require_once("www/config.inc.php");
	require_once("www/utils.inc.php");

	$basedir = dirname($_SERVER['SCRIPT_FILENAME']);
	$ruri = $_SERVER['PATH_INFO'];		/* Contains the path after "/content" */
	$m = explode('/', trim($ruri));
	$f = "$basedir/www/".$m[1].".php";

	/* Do not give access to *.inc.php files */
	if(preg_match('/.*\.inc\.php$/', $f))
		dispatch_return_404();

	$dispatch_cookie = true;		/* The header.inc.php will check to determine dispatched page */

	/* Dispatch control to the php-file matching the content part */
	if(file_exists($f)) {
		/* File found, give it control */
		include("$f");
	} else if($m[1] === "") {
		/* File empty, i.e. "/content[/]", show frontpage */
		include("$basedir/www/frontpage.php");
	} else {
		/* Otherwise we have no knowledge */
		dispatch_return_404();
	}
?>
