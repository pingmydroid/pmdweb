DROP TABLE IF EXISTS pmd_subcategory;
DROP TABLE IF EXISTS pmd_category;
DROP TABLE IF EXISTS pmd_queue;
DROP TABLE IF EXISTS pmd_msgcontent;
DROP TABLE IF EXISTS pmd_history;
DROP TABLE IF EXISTS pmd_severity;
DROP TABLE IF EXISTS pmd_type;
DROP TABLE IF EXISTS pmd_configvalue;
DROP TABLE IF EXISTS pmd_session;
DROP TABLE IF EXISTS pmd_user;
DROP TABLE IF EXISTS pmd_cert;

DROP SEQUENCE IF EXISTS pmd_history_seq;

CREATE TABLE pmd_category (
  id bigint NOT NULL CHECK (id >= 0 and id < 1::bigint << 32),
  name text,
  description text
);
CREATE UNIQUE INDEX xpmd_category_pkey ON pmd_category(id);

INSERT INTO pmd_category VALUES
  (0, 'PMDC_LOCAL', 'Local scope message'),
  (1, 'PMDC_MONITOR', 'Monitor system''s message'),
  (2, 'PMDC_LOG', 'Log message'),
  (3, 'PMDC_TRAFFIC', 'Traffic announcement'),
  (4, 'PMDC_PUBLIC', 'Public interest message'),
  (128, 'PMDC_COMMERCIAL', 'Commercial message'),
  (129, 'PMDC_SERVICE', 'Service message');

CREATE TABLE pmd_subcategory (
  cat bigint NOT NULL REFERENCES pmd_category(id),
  id bigint NOT NULL CHECK (id >= 0 and id < 1::bigint << 32),
  name text,
  description text
);
CREATE UNIQUE INDEX xpmd_subcategory_pkey ON pmd_subcategory(cat, id);
CREATE INDEX xpmd_subcategory_id ON pmd_subcategory(id);

INSERT INTO pmd_subcategory VALUES
  (0, 0, 'PMDC_LOCAL_NONE', 'Local unspecified message'),
  (0, 1, 'PMDC_LOCAL_BELL', 'Frontdoor bell is ringing'),
  (0, 2, 'PMDC_LOCAL_DINNER', 'Dinner is ready, stop gaming and eat'),
  (1, 0, 'PMDC_MONOTOR_NONE', 'Monitor unspecified message'),
  (1, 1, 'PMDC_MONOTOR_TEMPERATURE', 'Temperature monitor triggered'),
  (2, 0, 'PMDC_LOG_NONE', 'Log unspecified message'),
  (3, 0, 'PMDC_TRAFFIC_NONE', 'Traffic announcement unspecified'),
  (4, 0, 'PMDC_PUBLIC_NONE', 'Public interest unspecified message'),
  (128, 0, 'PMDC_COMMERCIAL_NONE', 'Commercial unspecified message'),
  (129, 0, 'PMDC_SERVICE_NONE', 'Service unspecified message'),
  (129, 1, 'PMDC_SERVICE_READYTOORDER', 'Customer is ready to order'),
  (129, 2, 'PMDC_SERVICE_CHECKPLEASE', 'Please present me with a cheque'),
  (129, 257, 'PMDC_SERVICE_QUEUEREQUEST', 'Request queue ticket'),
  (129, 258, 'PMDC_SERVICE_QUEUEASSIGN', 'Queue ticket assignment'),
  (129, 259, 'PMDC_SERVICE_QUEUESTATUS', 'Queue status');

CREATE TABLE pmd_type (
  id integer NOT NULL CHECK (id >= 0 and id < (1<<16)),
  name text,
  description text
);
CREATE UNIQUE INDEX xpmd_type_pkey ON pmd_type(id);

INSERT INTO pmd_type VALUES
  (0, 'PMDT_CATEGORY', 'Category'),
  (1, 'PMDT_SUBCATEGORY', 'Subcategory'),
  (2, 'PMDT_TEXT', 'Text'),
  (3, 'PMDT_RESOURCE', 'URI indicator'),
  (4, 'PMDT_UUID', 'Universal Unique ID'),
  (5, 'PMDT_TIMESTAMP', 'Timestamp'),
  (6, 'PMDT_CARDINAL', 'Unsigned number'),
  (7, 'PMDT_INTEGER', 'Signed number'),
  (8, 'PMDT_FRACTION', 'Fractional number'),
  (65531, 'PMDT_PADDING', 'Padding bytes'),
  (65532, 'PMDT_KEY', 'Encryption key'),
  (65533, 'PMDT_ENCRYPTED', 'Encrypted content'),
  (65534, 'PMDT_FINGERPRINT', 'Sender certificate fingerprint'),
  (65535, 'PMDT_SIGNATURE', 'RSA signature (must be last content)');

CREATE TABLE pmd_severity (
  id integer NOT NULL CHECK (id >= 0 and id < (1<<3)),
  name text,
  description text
);
CREATE UNIQUE INDEX xpmd_severity ON pmd_severity(id);

INSERT INTO pmd_severity VALUES
  (0, 'PMDS_EMERGENCY', 'Emergency message'),
  (1, 'PMDS_ALERT', 'Alert condition'),
  (2, 'PMDS_CRITICAL', 'Critical condition'),
  (3, 'PMDS_ERROR', 'Error condition'),
  (4, 'PMDS_WARNING', 'Warning condition'),
  (5, 'PMDS_NOTICE', 'Noticable condition'),
  (6, 'PMDS_INFO', 'Informational message'),
  (7, 'PMDS_DEBUG', 'Debugging message');

CREATE SEQUENCE pmd_history_seq;

CREATE TABLE pmd_history (
  id bigint NOT NULL DEFAULT nextval('pmd_history_seq'),
  usr text,         -- User who sent this message
  ts timestamp NOT NULL,
  severity integer NOT NULL REFERENCES pmd_severity(id),
  forcesilent boolean NOT NULL,
  uniid uuid NOT NULL,
  sigfp text,
  sent timestamp DEFAULT NULL
);
CREATE UNIQUE INDEX xpmd_history_pkey ON pmd_history(id);
CREATE INDEX xpmd_history_ts ON pmd_history(ts);

CREATE TABLE pmd_msgcontent (
  id bigint NOT NULL REFERENCES pmd_history(id) ON DELETE CASCADE,
  seq integer NOT NULL,  -- ordering of the content fields
  typ integer NOT NULL REFERENCES pmd_type(id),
  -- union start
  u_int1 bigint,    -- category, subcategory, integer, cardinal, fraction(dividend)
  u_int2 bigint,    -- fraction(divisor)
  u_ts timestamp,   -- timestamp
  u_txt text        -- text, resource, uuid, certificate/pkey
  -- union end
);
CREATE INDEX xpmd_msgcontent_id ON pmd_msgcontent(id, seq);

CREATE TABLE pmd_queue (
  id bigint NOT NULL REFERENCES pmd_history(id) ON DELETE CASCADE,
  ts timestamp,    -- Process sender aquire timestamp
  pid integer      -- PID of process handling this one
);
CREATE INDEX xpmd_queue_pid ON pmd_queue(pid, id);

-- Actual content of the configs
CREATE TABLE pmd_configvalue (
  ts timestamp NOT NULL DEFAULT now(),
  content text
);
CREATE UNIQUE INDEX xpmd_configvalue_ts ON pmd_configvalue(ts);

CREATE TABLE pmd_session (
  id text NOT NULL,
  dat text,
  ts timestamp NOT NULL
);
CREATE UNIQUE INDEX xpmd_session_id ON pmd_session(id);

CREATE TABLE pmd_user (
 id text NOT NULL,
 salt text NOT NULL,
 pwhash text NOT NULL,
 priv integer NOT NULL DEFAULT 0
);
CREATE UNIQUE INDEX xpmd_user_id ON pmd_user(id);

INSERT INTO pmd_user VALUES
  ('admin', random()::text, '', -1),
  ('demo', random()::text, '', 3);

UPDATE pmd_user SET pwhash=md5(salt||id);

CREATE TABLE pmd_cert (
  fp text NOT NULL,
  txt text NOT NULL,
  cn text NOT NULL,
  subj text NOT NULL,
  needpw boolean NOT NULL,
  validfrom timestamp with time zone NOT NULL,
  validto timestamp with time zone NOT NULL,
  cert TEXT NOT NULL,
  pkey TEXT NOT NULL
);

CREATE UNIQUE INDEX xpmd_cert_fp ON pmd_cert(fp);
