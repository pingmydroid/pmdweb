# vim: syn=make
# This post_subst is to create target files from .in files after configure has
# run. It is layed out in a way such that all paths are expanded all the way
# and no intermediate ${prefix} or other un-expanded entities occur.
#
# Configure cannot solve this directly because if no prefix is given on the
# command-line, then the assignment of the prefix may be delayed until after
# our checks and then it would expand to "NONE" instead of the default "/usr".
#
# Here, in the makefile, all paths are expanded in the sed expression and the
# substitution will be as we want it to be, an absolute path.
#
postsubst = sed \
		-e 's,[@]pkgdatadir[@],$(pkgdatadir),g' \
		-e 's,[@]oldincludedir[@],$(oldincludedir),g' \
		-e 's,[@]pdfdir[@],$(pdfdir),g' \
		-e 's,[@]psdir[@],$(psdir),g' \
		-e 's,[@]dvidir[@],$(dvidir),g' \
		-e 's,[@]htmldir[@],$(htmldir),g' \
		-e 's,[@]docdir[@],$(docdir),g' \
		-e 's,[@]datadir[@],$(datadir),g' \
		-e 's,[@]infodir[@],$(infodir),g' \
		-e 's,[@]localedir[@],$(localedir),g' \
		-e 's,[@]mandir[@],$(mandir),g' \
		-e 's,[@]libdir[@],$(libdir),g' \
		-e 's,[@]libexecdir[@],$(libexecdir),g' \
		-e 's,[@]bindir[@],$(bindir),g' \
		-e 's,[@]sbindir[@],$(sbindir),g' \
		-e 's,[@]exec_prefix[@],$(exec_prefix),g' \
		-e 's,[@]includedir[@],$(includedir),g' \
		-e 's,[@]localstatedir[@],$(localstatedir),g' \
		-e 's,[@]datarootdir[@],$(datarootdir),g' \
		-e 's,[@]sharedstatedir[@],$(sharedstatedir),g' \
		-e 's,[@]sysconfdir[@],$(sysconfdir),g' \
		-e 's,[@]prefix[@],$(prefix),g' \
		-e 's,[@]PACKAGE[@],$(PACKAGE),g' \
		-e 's,[@]VERSION[@],$(VERSION),g' \
		-e 's,[@]IPROUTE2[@],$(IPROUTE2),g' \
		-e 's,[@]PMDSEND[@],$(PMDSEND),g'
